# Code Samples

This is an example of a project I've worked on. It's a full-stack application using React/Redux and Node.js.
The project was built for a charity and the code has been anonymized.

## Key features:

### Front-end React application:

- Redux
- Immutable.js
- SASS
- Webpack

### Back-end Node application:

- hapi
- MongoDB
- Stripe for payments
