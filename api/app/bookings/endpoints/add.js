const mongoose = require('mongoose');
const boom = require('boom');
const joi = require('joi');
const get = require('lodash/get');

/*
Two endpoints with a common handler:
  (1) POST /bookings
  (2) POST /admin/bookings

Endpoints payload's are validated differently by `joi` to create different booking contexts
Endpoint (2) requires 'admin' authentication
*/

module.exports = (Booking, stripe, emitter) => {
  const add = async (request, reply) => {
    const Performance = mongoose.model('Performance');
    let data = request.payload;

    const replyWithError = error => reply(boom.badRequest(error));

    // Create booking with status 'pending' and reserve seats
    // If seats already reserved, insert will fail;
    try {
      const status = get(data.admin, 'reservation') ? 'complete' : 'pending';
      data = await Booking.create({ ...data, status, seatsReserved: true });
    } catch (e) {
      return e.code === 11000 ? reply(boom.conflict()) : reply(boom.badRequest(e));
    }

    // If not a reservation…
    if (get(data.admin, 'reservation') !== true) {
      // Generate `reference` by incrementing `bookings` in performance document
      try {
        const { code, bookings } = await Performance.findByIdAndUpdate(
          data.performance,
          { $inc: { bookings: 1 } },
          { new: true },
        );

        const $set = {
          reference: `${code}${bookings}`,
          status: request.payload.token ? 'pending' : 'complete',
        };

        data = await Booking.findByIdAndUpdate(data._id, { $set }, { new: true })
          .populate({
            path: 'production',
            select: ['title', 'shortTitle', 'author'],
          })
          .lean();
      } catch (e) {
        return replyWithError(e);
      }

      // If payment token, create charge
      if (request.payload.token) {
        try {
          const { id } = await stripe.charges.create({
            amount: data.total * 100,
            currency: 'gbp',
            source: request.payload.token,
            description: `Tickets: #${data.reference}`,
            receipt_email: data.email,
          });
          try {
            data = await Booking.findByIdAndUpdate(
              data._id,
              { $set: { status: 'complete', charge: id } },
              { new: true },
            )
              .populate({
                path: 'production',
                select: ['title', 'shortTitle', 'author'],
              })
              .lean();

            // Emit 'newTickets' even to trigger email delivery
            // If no email is present, event will be ignored
            emitter.emit('newTickets', data);
          } catch (e) {
            return replyWithError(e);
          }
        } catch (chargeError) {
          try {
            const $set = {
              status: 'failed',
              seatsReserved: false,
              chargeError: chargeError.code,
            };
            await Booking.findByIdAndUpdate(data._id, { $set }, { new: true });
            return reply(boom.badRequest(chargeError));
          } catch (e) {
            return replyWithError(e);
          }
        }
      } else if (data.email) {
        emitter.emit('newTickets', data);
      }
    }

    return reply(data).code(201);
  };

  return [
    {
      method: 'POST',
      path: '/bookings',
      config: {
        validate: {
          payload: joi.object({
            production: joi.string().required(),
            performance: joi.string().required(),
            tickets: joi
              .object({
                full: joi.number().required(),
                family: joi.number().required(),
                concessions: joi.number().required(),
              })
              .required(),
            seats: joi.array().required(),
            seatsString: joi.string().required(),
            name: joi.string().required(),
            telephone: joi.string().required(),
            email: joi.string().required(),
            total: joi.number().required(),
            cardPayment: joi.boolean().required(),
            token: joi.string(),
          }),
          options: {
            abortEarly: false,
          },
        },
      },
      handler: add,
    },
    {
      method: 'POST',
      path: '/admin/bookings',
      config: {
        auth: 'admin',
        validate: {
          payload: joi.object({
            production: joi.string().required(),
            performance: joi.string().required(),
            tickets: joi
              .object({
                full: joi.number().required(),
                family: joi.number().required(),
                concessions: joi.number().required(),
              })
              .required(),
            seats: joi.array().required(),
            seatsString: joi.string().required(),
            name: joi.string().required(),
            telephone: joi.string(),
            email: joi.string(),
            total: joi.number().required(),
            cardPayment: joi.boolean().required(),
            token: joi.string(),
            admin: joi.object().required(),
          }),
          options: {
            abortEarly: false,
          },
        },
      },
      handler: add,
    },
  ];
};
