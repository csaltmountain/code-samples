const ObjectId = require('mongoose').Types.ObjectId;
const boom = require('boom');
const joi = require('joi');

/*
Two endpoints:
  (1) GET /bookings/availability
  (2) GET /admin/bookings/availability

Endpoints payload's are validated differently by `joi` to create different contexts
Endpoint (2) requires 'admin' authentication
*/

module.exports = (Booking) => {
  const generateSeatingPlan = (bookings) => {
    const bookedSeats = bookings.reduce((acc, { _id, reference, name, seats }) => (
      [...acc, ...seats.map(({ row, seat }) => ({ _id, reference, name, row, seat }))]
    ), []);

    const rowIndexes = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N'];
    const availability = rowIndexes.map(row => Array(rowIndexes.length)
      .fill({})
      .map((seat, index) => ({
        row,
        seat: 13 - index,
        status: 'available',
      })));

    if (bookedSeats.length) {
      bookedSeats.forEach((item) => {
        availability[rowIndexes.indexOf(item.row)][13 - item.seat] = {
          ...item,
          status: 'booked',
        };
      });
    }

    return {
      availability,
      count: bookedSeats.length,
    };
  };

  return [
    {
      method: 'GET',
      path: '/bookings/availability',
      config: {
        validate: {
          query: {
            performance: joi.string().required(),
          },
        },
      },
      handler: async (request, reply) => {
        try {
          const { performance } = request.query;
          const bookings = await Booking.find({ performance, $where: 'this.seatsReserved' }).select(['-_id', 'seats']).lean();
          return reply(generateSeatingPlan(bookings).availability);
        } catch (e) {
          return reply(boom.badRequest(e));
        }
      },
    },
    {
      method: 'GET',
      path: '/admin/bookings/availability',
      config: {
        auth: 'admin',
        validate: {
          query: {
            production: joi.string().required(),
          },
        },
      },
      handler: async (request, reply) => {
        try {
          const { production } = request.query;
          const performances = await Booking.aggregate([
            { $match: { production: ObjectId(production), seatsReserved: true } },
            {
              $group: {
                _id: '$performance',
                bookings: {
                  $push: {
                    _id: '$_id',
                    reference: '$reference',
                    name: '$name',
                    seats: '$seats',
                  },
                },
              },
            },
            { $sort: { _id: 1 } },
          ]);

          const seatingPlans = performances.map(item => ({
            _id: item._id,
            ...generateSeatingPlan(item.bookings),
          }));

          return reply(seatingPlans.sort());
        } catch (e) {
          return reply(boom.badRequest(e));
        }
      },
    },
  ];
};
