const mongoose = require('mongoose');
const boom = require('boom');

module.exports = Booking => ({
  method: 'GET',
  path: '/bookings',
  config: {
    auth: 'admin',
  },
  handler: async (request, reply) => {
    try {
      const production = mongoose.Types.ObjectId(request.query.production);

      const bookings = await Booking.find({ production })
        .select(['_id', 'reference', 'name', 'status', 'admin', 'seatsReserved', 'performance', 'tickets', 'seatsString'])
        .sort({ $natural: -1 })
        .lean();

      const group = (_id = null) => ({
        _id,
        revenue: { $sum: '$total' },
        tickets: { $sum: { $size: '$seats' } },
        bookings: { $sum: 1 },
      });

      const [all] = await Booking.aggregate([
        { $match: { production, seatsReserved: true } },
        { $group: group('Total') },
      ]);

      const aggregate = (name, match) => new Promise((resolve, reject) => {
        Booking.aggregate([
          { $match: { ...match, production, seatsReserved: true } },
          { $group: group(name) },
        ]).then(([data]) => resolve(data)).catch(reject);
      });

      reply({
        bookings: bookings.map(booking => ({
          ...booking,
          platform: booking.admin ? booking.admin.user : 'online',
          reservation: booking.admin && booking.admin.reservation,
          createdAt: new mongoose.Types.ObjectId(booking._id).getTimestamp(),
          admin: undefined,
        })),
        statistics: [
          await aggregate('Online', { admin: { $exists: false } }) || { _id: 'Online' },
          await aggregate('Telephone', { 'admin.user': 'telephone' }) || { _id: 'Telephone' },
          await aggregate('In-person', { 'admin.user': 'terminal' }) || { _id: 'In-person' },
          all,
        ],
      });
    } catch (e) {
      reply(boom.badRequest(e));
    }
  },
});
