const mongoose = require('mongoose');
const boom = require('boom');

module.exports = (Booking, stripe, emitter) => ({
  method: ['GET', 'PATCH'],
  path: '/bookings/{_id}',
  config: {
    auth: 'admin',
  },
  handler: async (request, reply) => {
    try {
      const booking = await Booking.findById(request.params._id)
        .populate({
          path: 'production',
          select: ['title', 'shortTitle', 'author'],
        })
        .populate('emails')
        .lean();

      if (request.method === 'patch') {
        emitter.emit('resendTickets', booking);
        return reply({ event: 'resendTickets' });
      }

      if (booking.charge) {
        try {
          booking.charge = await stripe.charges.retrieve(booking.charge);
        } catch (e) {
          booking.charge = { error: 'could not fetch charge' };
        }
      }

      return reply({
        ...booking,
        platform: booking.admin ? booking.admin.user : 'online',
        reservation: booking.admin && booking.admin.reservation,
        createdAt: new mongoose.Types.ObjectId(booking._id).getTimestamp(),
      });
    } catch (e) {
      return reply(boom.badRequest(e));
    }
  },
});
