const boom = require('boom');

module.exports = (Booking, generatePDF) => ({
  method: 'GET',
  path: '/bookings/{_id}/tickets',
  config: {
    auth: 'admin',
  },
  handler: async (request, reply) => {
    try {
      const booking = await Booking.findById(request.params._id)
        .select(['name', 'seats', 'production', 'performance', 'reference', 'tickets'])
        .populate({
          path: 'production',
          select: ['title', 'shortTitle', 'author'],
        })
        .lean();

      return reply(await generatePDF(booking)).header('content-type', 'application/pdf');
    } catch (e) {
      return reply(boom.badRequest(e));
    }
  },
});
