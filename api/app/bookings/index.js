const EventEmitter = require('events').EventEmitter;
const stripe = require('stripe')(process.env.STRIPE_SK);

const model = require('./model');
const browse = require('./endpoints/browse');
const add = require('./endpoints/add');
const read = require('./endpoints/read');
const tickets = require('./endpoints/tickets');
const availability = require('./endpoints/availability');

const generatePDF = require('../../libs/tickets').generatePDF;

const emitter = new EventEmitter();

module.exports = {
  routes: [
    browse(model),
    ...add(model, stripe, emitter),
    read(model, stripe, emitter),
    tickets(model, generatePDF),
    ...availability(model),
  ],
  emitter,
};
