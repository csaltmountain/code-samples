const mongoose = require('mongoose');

const bookingsSchema = new mongoose.Schema({
  reference: {
    type: String,
    default() {
      return this._id;
    },
  },
  production: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Production',
  },
  performance: {
    type: String,
    ref: 'Performance',
  },
  tickets: {
    full: Number,
    family: Number,
    concessions: Number,
  },
  seats: [{
    row: String,
    seat: String,
  }],
  seatsString: String,
  seatsReserved: Boolean,
  name: String,
  telephone: String,
  email: String,
  total: Number,
  cardPayment: Boolean,
  admin: {
    user: mongoose.Schema.Types.Mixed,
    noEmail: Boolean,
    reservation: Boolean,
    notes: String,
  },
  status: String,
  charge: String,
  chargeError: String,
}, { versionKey: false, timestamps: true });

bookingsSchema.index(
  {
    performance: 1,
    'seats.row': 1,
    'seats.seat': 1,
  },
  {
    unique: true,
    partialFilterExpression: {
      seatsReserved: true,
    },
  },
);

bookingsSchema.index(
  { reference: 1 },
  {
    unique: true,
    partialFilterExpression: {
      reference: true,
    },
  },
);

bookingsSchema.index(
  { performance: 1, seatsReserved: 1 },
);

bookingsSchema.virtual('emails', {
  ref: 'Email',
  localField: 'reference',
  foreignField: 'bookingReference',
});

module.exports = mongoose.model('Booking', bookingsSchema);
