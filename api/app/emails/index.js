const Postmark = require('postmark');

const Email = require('./model');
const sendTickets = require('./listeners/sendTickets');

const bookingEmitter = require('../bookings').emitter;
const generatePDF = require('../../libs/tickets').generatePDF;

const postmark = new Postmark.Client('62a8825f-9a89-40ab-b489-56883d26c426');

module.exports = () => {
  sendTickets(bookingEmitter, Email, generatePDF, postmark);
};
