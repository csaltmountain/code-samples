const moment = require('moment');

module.exports = (bookingEmitter, Email, generatePDF, postmark) => {
  const sendTickets = async (type, booking) => {
    if (booking.email) {
      let deliveryReport;
      let pdf;

      try {
        pdf = await generatePDF(booking);
      } catch (e) {
        console.log(e);
      }

      try {
        deliveryReport = await postmark.sendEmailWithTemplate({
          From: 'no-reply@saltmountainhq.com',
          To: booking.email,
          TemplateId: 3190783,
          TemplateModel: {
            name: booking.name,
            production: booking.production.shortTitle || booking.production.title,
            performance: moment.utc(booking.performance).format('dddd Do MMMM YYYY [at] HH:mm'),
            ref: booking.reference,
            ticketsFull: booking.tickets.full || 0,
            ticketsFamily: booking.tickets.family || 0,
            ticketsConcessions: booking.tickets.concessions || 0,
            total: `£${booking.total}`,
          },
          Attachments: [
            {
              Name: `${booking.reference}.pdf`,
              Content: pdf.toString('base64'),
              ContentType: 'application/pdf',
            },
          ],
        });

        try {
          await Email.create({
            status: 'complete',
            type,
            bookingReference: booking.reference,
            postmark: deliveryReport,
          });
        } catch (e) {
          console.log(e);
        }
      } catch (postmarkError) {
        try {
          await Email.create({
            status: 'failed',
            type,
            bookingReference: booking.reference,
            postmark: postmarkError,
          });
        } catch (e) {
          console.log(e);
        }
      }
    }
  };

  bookingEmitter.on('newTickets', booking => sendTickets('newTickets', booking));
  bookingEmitter.on('resendTickets', booking => sendTickets('resendTickets', booking));
};
