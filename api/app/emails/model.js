const mongoose = require('mongoose');

const emailSchema = new mongoose.Schema({
  status: {
    type: String,
    requiried: true,
  },
  type: {
    type: String,
    requiried: true,
  },
  bookingReference: {
    type: String,
  },
  postmark: {},
}, { versionKey: false, timestamps: true });

module.exports = mongoose.model('Email', emailSchema);
