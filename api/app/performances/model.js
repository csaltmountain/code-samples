const mongoose = require('mongoose');

const performanceSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
  bookings: {
    type: Number,
    required: true,
  },
}, { versionKey: false });

module.exports = mongoose.model('Performance', performanceSchema);
