const defaultHandlers = require('../../libs/defaultHandlers');

const routes = require('./routes');
const model = require('./model');

module.exports = routes({
  ...defaultHandlers(model, {
    select: ['_id', 'title', 'author', 'description', 'openingNight', 'closingNight'],
    sort: { openingNight: 1 },
  }),
});
