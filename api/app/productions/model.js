const mongoose = require('mongoose');

const productionsSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  shortTitle: {
    type: String,
  },
  author: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  openingNight: {
    type: String,
    required: true,
  },
  closingNight: {
    type: String,
    required: true,
  },
  performances: [String],
  concessions: [String],
  ticketPrices: {
    adults: {
      type: Number,
      required: true,
    },
    concessions: {
      type: Number,
      required: true,
    },
  },
}, { versionKey: false });

module.exports = mongoose.model('Production', productionsSchema);
