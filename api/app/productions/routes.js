module.exports = ({ browse, read }) => [
  {
    method: 'GET',
    path: '/productions',
    handler: browse,
  },
  {
    method: 'GET',
    path: '/productions/{_id}',
    handler: read,
  },
];
