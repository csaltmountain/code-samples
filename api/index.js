const crypto = require('crypto');
const mongoose = require('mongoose');
const Hapi = require('hapi');

const customAuth = require('./libs/customAuth');
const productions = require('./app/productions');
const performances = require('./app/performances');
const bookings = require('./app/bookings');
require('./app/emails')();

const server = new Hapi.Server();

// Mongoose Config
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {
  useMongoClient: true,
});

server.connection({
  port: process.env.PORT,
  host: '0.0.0.0',
});

server.register(customAuth, () => {
  server.auth.strategy('check', 'g-suite-domain', (domain, callback) =>
    domain === 'saltmountainhq.com' ? callback(null, domain) : callback(null, 'user'),
  );

  server.auth.strategy('admin', 'g-suite-domain', (domain, callback) =>
    domain === 'saltmountainhq.com' ? callback(null, domain) : callback(true),
  );

  server.route({
    method: 'GET',
    path: '/checkToken',
    config: {
      auth: 'check',
    },
    handler(request, reply) {
      reply();
    },
  });

  server.route([...productions, ...performances, ...bookings.routes]);
});

server.ext('onPreResponse', (request, reply) => {
  if (request.response.source) {
    const etag = crypto
      .createHash('md5')
      .update(JSON.stringify(request.response.source), 'utf8')
      .digest('hex');
    request.response.header('ETag', etag);
  }
  return reply.continue();
});

server.start((err) => {
  if (err) {
    throw err;
  }
  console.log(`Server running at: ${server.info.uri}`);
  console.log('DB:', process.env.MONGODB_URI);
});

module.exports = server;
