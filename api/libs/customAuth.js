const boom = require('boom');
const { OAuth2Client } = require('google-auth-library');

const CLIENT_ID = '35581611685-rg5v1skfhpddm26jb9l6h420171seuqa.apps.googleusercontent.com';
const client = new OAuth2Client(CLIENT_ID);

const customAuth = {
  register: (plugin, options, next) => {
    plugin.auth.scheme('g-suite-domain', (server, validateFunc) => ({
      async authenticate(request, reply) {
        try {
          const token = request.headers.authorization && request.headers.authorization.slice(7);
          if (!token) {
            throw Error();
          }

          const ticket = await client.verifyIdToken({
            idToken: token,
            audience: CLIENT_ID,
          });

          return validateFunc(ticket.getPayload().hd, (err, credentials) =>
            err ? reply(boom.forbidden()) : reply.continue({ credentials }),
          );
        } catch (e) {
          return reply(boom.forbidden(e));
        }
      },
    }));
    next();
  },
};

customAuth.register.attributes = {
  name: 'customAuth',
  version: '1.0.0',
};

module.exports = customAuth;
