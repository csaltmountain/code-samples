const boom = require('boom');

module.exports = (Model, {
  query: defaultQuery,
  select: defaultSelect,
  sort: defaultSort,
} = {}) => ({
  async browse(request, reply) {
    try {
      const query = (request.query.query && JSON.parse(request.query.query)) || defaultQuery || {};
      const select = (request.query.select && request.query.select.split(',')) || defaultSelect || {};
      const sort = (request.query.sort && JSON.parse(request.query.sort)) || defaultSort || {};
      reply(await Model.find(query || {}).select(select).sort(sort));
    } catch (e) {
      reply(boom.badRequest(e));
    }
  },

  async read(request, reply) {
    try {
      reply(await Model.findById(request.params._id));
    } catch (e) {
      reply(boom.badRequest(e));
    }
  },
});
