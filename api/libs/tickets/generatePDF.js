const fs = require('fs');
const path = require('path');
const qrcode = require('qrcode');
const moment = require('moment');
const puppeteer = require('puppeteer');

const generateQRCode = async input =>
  new Promise((resolve, reject) =>
    qrcode.toDataURL(input, (err, url) =>
    (err ? reject(err) : resolve(url))));

module.exports = data => new Promise((resolve, reject) => {
  (async () => {
    let html;

    try {
      const {
        name,
        seats,
        performance,
        production: {
          title,
          shortTitle,
          author,
        },
        reference,
        tickets: {
          full,
          family,
          concessions,
        },
      } = data;

      // console.log(request.payload);

      const date = moment.utc(performance);
      const dateFormatted = date.format('dddd Do MMMM YYYY');
      const timeFormatted = date.format('HH:mm');

      const QRCode = await generateQRCode(reference);

      const tickets = seats.map((seat, index, array) => {
        const isFirst = index === 0;
        const isLast = index === array.length - 1;
        const seatString = `${seat.row}${seat.seat}`;
        const openinghtml = '<div class="page-break"><div class="row">';
        const mainHtml = `<div class="col-6 mt-5"><div class="card" style="border-color: #343a40"><div class="card-body text-center"><div class="h5 mb-3"><i>Salt Mountain Presents</i></div><div class="h3 card-title">${shortTitle || title}</div><div class="h5 card-subtitle text-muted mb-3">${author}</div><div class="display-3 mb-3"><span class="badge badge-dark">${seatString}</span></div><div class="h3">${dateFormatted}<br/>${timeFormatted}</div></div><div class="card-footer d-flex justify-content-between align-items-center" style="line-height: 1;"><div>${reference}<br /><small>${name}</small></div><div class="d-flex align-items-center"><div class="mr-3 text-right">Full Price: ${full} / Family: ${family} / Senior Citizens: ${concessions}<br />Ticket ${index + 1} of ${array.length}</div><img height="31" width="31" src="${QRCode}" alt="${QRCode}"></div></div></div></div>`;
        const closinghtml = '</div></div>';

        if (isFirst) {
          return openinghtml + mainHtml;
        }

        if ((index + 1) % 6 === 0 && !isLast) {
          return mainHtml + closinghtml + openinghtml;
        }

        if (isLast) {
          return mainHtml + closinghtml;
        }

        return mainHtml;
      }).join('');

      const bootstrap = fs.readFileSync(path.resolve(__dirname, './vendor/bootstrap/bootstrap.min.css'), 'utf-8');

      html = `<!DOCTYPE html><html><head><meta charset="utf-8"><title>Salt Mountain | Your Tickets</title><style>${bootstrap}</style><style type="text/css">@page {margin: 1cm;}body,html {font-family: Ubuntu;margin: 0;font-size: 10px;}</style><style type="text/css" media="print">.page-break{page-break-after: always;page-break-inside: avoid;}</style></head><body><div class="row align-items-center mb-4 "><div class="col"><div class="h1">Salt Mountain</div><div class="h2">Your Tickets</div></div><div class="col text-center"><img height="100" width="100" src="${QRCode}" alt="${QRCode}"></div><div class="col text-right"><div class="h3">${reference}<br /><i>${name}</i></div></div></div><hr><div class="display-4 text-center mt-5">${dateFormatted}<br/>${timeFormatted}</div>${tickets}</body></html>`;
    } catch (e) {
      reject(e);
    }

    try {
      const browser = await puppeteer.launch();
      const page = await browser.newPage();
      await page.setContent(html);
      const pdf = await page.pdf({ format: 'A4', printBackground: true });
      await browser.close();
      resolve(pdf);
    } catch (e) {
      reject(e);
    }
  })();
});
