import axios, { get } from 'axios';

import { LOGIN_SUCCEEDED, LOGIN_FAILED, SIGN_OUT_SUCCEEDED, SIGN_OUT_FAILED } from './actions';

export const login = () => (dispatch) => {
  if (window.location.hostname !== process.env.ADMIN_HOSTNAME) {
    dispatch({
      type: LOGIN_SUCCEEDED,
    });
  } else {
    gapi.load('auth2', () => {
      gapi.auth2
        .init({
          client_id: '35581611685-rg5v1skfhpddm26jb9l6h420171seuqa.apps.googleusercontent.com',
        })
        .then((GoogleAuth) => {
          if (GoogleAuth.isSignedIn.get()) {
            const googleUser = GoogleAuth.currentUser.get();

            axios.defaults.headers.Authorization = `Bearer ${
              googleUser.getAuthResponse().id_token
            }`;

            get('/checkToken')
              .then(() => {
                dispatch({
                  type: LOGIN_SUCCEEDED,
                  payload: {
                    user: googleUser.getBasicProfile().getName(),
                  },
                });
              })
              .catch(() =>
                dispatch({
                  type: LOGIN_FAILED,
                }),
              );
          } else {
            GoogleAuth.signIn({
              ux_mode: 'redirect',
            });
          }
        })
        .catch(() =>
          dispatch({
            type: LOGIN_FAILED,
          }),
        );
    });
  }
};

export const signOut = () => (dispatch) => {
  gapi.load('auth2', () => {
    gapi.auth2
      .init({
        client_id: '35581611685-rg5v1skfhpddm26jb9l6h420171seuqa.apps.googleusercontent.com',
      })
      .then((GoogleAuth) => {
        GoogleAuth.signOut();
        dispatch({
          type: SIGN_OUT_SUCCEEDED,
        });
      })
      .catch(() =>
        dispatch({
          type: SIGN_OUT_FAILED,
        }),
      );
  });
};
