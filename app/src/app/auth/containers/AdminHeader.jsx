import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { Logo } from '../../elements';
import { signOut } from '../actionCreators';

function AdminHeader({ admin, location, dispatchSignOut }) {
  return admin ? (
    <nav className="navbar navbar-light bg-light navbar-expand mb-4 justify-content-center">
      <div className="navbar-brand d-none d-sm-block">
        <Logo width="30" height="30" className="d-inline-block align-top mr-2" />
        Box Office Admin
      </div>
      <ul className="navbar-nav ml-sm-auto">
        <li className={location.pathname === '/form' ? 'nav-item active' : 'nav-item'}>
          <Link className="nav-link" to="/form">
            Booking Form
          </Link>
        </li>
        <li className={location.pathname.startsWith('/bookings') ? 'nav-item active' : 'nav-item'}>
          <Link className="nav-link" to="/bookings">
            Bookings
          </Link>
        </li>
        <li
          className={location.pathname.startsWith('/availability') ? 'nav-item active' : 'nav-item'}
        >
          <Link className="nav-link" to="/availability">
            Availability
          </Link>
        </li>
      </ul>
      <div className="badge badge-info mr-2">You are signed in as {admin}</div>
      <button className="btn btn-sm btn-outline-info" onClick={dispatchSignOut}>
        Sign out
      </button>
    </nav>
  ) : null;
}

AdminHeader.propTypes = {
  admin: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  location: PropTypes.objectOf(PropTypes.string).isRequired,
  dispatchSignOut: PropTypes.func.isRequired,
};

AdminHeader.defaultProps = {};

export default connect(
  state => ({
    admin: state.getIn(['auth', 'admin']),
  }),
  dispatch => ({
    dispatchSignOut: () => dispatch(signOut()),
  }),
)(AdminHeader);
