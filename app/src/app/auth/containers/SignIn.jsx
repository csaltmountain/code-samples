import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { login } from '../actionCreators';

class SignIn extends React.Component {
  componentDidMount() {
    this.props.dispatchLogin();
  }

  render() {
    return <div>SignIn</div>;
  }
}

SignIn.propTypes = {
  dispatchLogin: PropTypes.func.isRequired,
};

SignIn.defaultProps = {};

export default connect(
  state => ({
    admin: state.getIn(['auth', 'admin']),
    succeeded: state.getIn(['auth', 'succeeded']),
  }),
  dispatch => ({
    dispatchLogin: () => dispatch(login()),
  }),
)(SignIn);
