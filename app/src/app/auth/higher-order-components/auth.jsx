import React from 'react';
import { Redirect } from 'react-router-dom';

function auth(WrappedComponent) {
  function Auth() {
    if (window.location.hostname === process.env.ADMIN_HOSTNAME) {
      return <WrappedComponent />;
    }
    return <Redirect to="/" />;
  }

  return Auth;
}

export default auth;
