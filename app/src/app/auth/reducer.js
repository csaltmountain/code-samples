import { fromJS } from 'immutable';

import { LOGIN_SUCCEEDED, SIGN_OUT_SUCCEEDED } from './actions';

import initialState from './initialState';

export default (state = fromJS(initialState()), { type, payload }) => {
  switch (type) {
    case LOGIN_SUCCEEDED: {
      return state.withMutations(map =>
        map.set('admin', payload ? payload.user : false).set('succeeded', true),
      );
    }

    case SIGN_OUT_SUCCEEDED: {
      return state.set('admin', false);
    }

    default: {
      return state;
    }
  }
};
