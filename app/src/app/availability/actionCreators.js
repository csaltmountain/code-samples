import { get } from 'axios';

import {
  GET_AVAILABILITY_SUCCEEDED,
  GET_AVAILABILITY_FAILED,
} from './actions';

export default production => (dispatch) => {
  get(`/admin/bookings/availability?production=${production}`)
    .then(({ data }) => dispatch({
      type: GET_AVAILABILITY_SUCCEEDED,
      payload: { data },
    }))
    .catch(error => dispatch({
      type: GET_AVAILABILITY_FAILED,
      payload: { error },
    }));
};
