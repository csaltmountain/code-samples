/* eslint react/forbid-prop-types: "off" */
import React from 'react';
import PropTypes from 'prop-types';
import ChartJS from 'chart.js';

class Chart extends React.Component {
  componentDidMount() {
    this.drawChart();
  }

  componentDidUpdate() {
    this.drawChart();
  }

  drawChart() {
    ChartJS.defaults.global.title.fontFamily = 'Ubuntu';
    ChartJS.defaults.global.title.fontSize = 20;

    this.chart = new ChartJS(this.canvas, this.props.options);
  }

  render() {
    const { options, ...passThroughProps } = this.props;
    return (
      <div {...passThroughProps}>
        <canvas ref={(canvas) => { this.canvas = canvas; }} />
      </div>
    );
  }
}

Chart.propTypes = {
  options: PropTypes.object.isRequired,
};

export default Chart;
