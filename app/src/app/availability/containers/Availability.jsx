import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { Helmet } from 'react-helmet';
import moment from 'moment';

import auth from '../../auth';
import getAvailability from '../actionCreators';

import { SeatingPlan } from '../../form';
import Chart from '../components/Chart';
import ProductionFilter from '../../production-filter';

import '../styles/seating-plan.scss';

const chartOptions = (labels, sold) => ({
  type: 'horizontalBar',
  data: {
    labels,
    datasets: [
      {
        data: sold,
        backgroundColor: '#868e96',
      },
    ],
  },
  options: {
    title: {
      display: true,
      text: 'Tickets Sold',
    },
    legend: {
      display: false,
    },
    scales: {
      xAxes: [{
        stacked: true,
        ticks: {
          min: 0,
          max: 169,
          stepSize: 10,
        },
      }],
      yAxes: [{
        stacked: true,
      }],
    },
  },
});

function Availability({ data, dispatchGetAvailability }) {
  let content;

  const {
    labels,
    sold,
    cards,
  } = data.toJS().reduce((acc, curr) => {
    const day = moment(curr._id).format('ddd Do HH:mm');
    acc.labels.push(day);
    acc.sold.push(curr.count);
    acc.remaining.push(169 - curr.count);
    acc.cards.push(
      <div key={curr._id} className="col-md-6 mb-4">
        <div className="card">
          <div className="card-body text-center">
            <div className="h5 mb-3">{day}</div>
            <div className="d-flex justify-content-around mb-3">
              <div>
                <div className="display-4 text-secondary">{curr.count}</div>
                Sold
              </div>
              <div>
                <div className="display-4 text-primary">{169 - curr.count}</div>
                Remaining
              </div>
            </div>
            <SeatingPlan availability={curr.availability} />
          </div>
        </div>
      </div>,
    );
    return acc;
  }, { labels: [], sold: [], remaining: [], cards: [] });

  if (data.size === 0) {
    content = (
      <div className="text-center">
        <i className="fa fa-spinner fa-pulse fa-4x fa-fw" />
      </div>
    );
  } else {
    content = (
      <div>
        <Chart options={chartOptions(labels, sold)} className="mb-5" />
        <div className="row">
          {cards}
        </div>
      </div>
    );
  }

  return (
    <div className="availability container">
      <Helmet>
        <title>{'Availability — Salt Mountain Box Office'}</title>
      </Helmet>
      <h1 className="text-center mb-5">Availability</h1>
      <ProductionFilter
        onSelect={production => dispatchGetAvailability(production)}
      />
      {content}
    </div>
  );
}

Availability.propTypes = {
  data: ImmutablePropTypes.list,
  dispatchGetAvailability: PropTypes.func.isRequired,
};

Availability.defaultProps = {
  data: List(),
};

export default compose(
  auth,
  connect(
    state => ({
      data: state.getIn(['availability', 'data']),
    }),
    dispatch => ({
      dispatchGetAvailability: production => dispatch(getAvailability(production)),
    }),
  ),
)(Availability);
