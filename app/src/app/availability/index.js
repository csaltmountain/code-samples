import Availability from './containers/Availability';
import reducer from './reducer';

export { Availability as default, reducer };
