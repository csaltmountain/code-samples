import { Map, fromJS } from 'immutable';
import {
  GET_AVAILABILITY_SUCCEEDED,
} from './actions';

export default (state = Map(), { type, payload }) => {
  switch (type) {
    case GET_AVAILABILITY_SUCCEEDED: {
      return state.set('data', fromJS(payload.data));
    }

    default: {
      return state;
    }
  }
};
