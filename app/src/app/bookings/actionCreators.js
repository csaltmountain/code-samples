import { get } from 'axios';
import Fuse from 'fuse.js';

import {
  GET_BOOKINGS_SUCCEEDED,
  GET_BOOKINGS_FAILED,
  GET_BOOKING_SUCCEEDED,
  GET_BOOKING_FAILED,
  CLEAR_BOOKING,
  FILTER_BOOKINGS,
} from './actions';

export const getBookings = production => (dispatch) => {
  get(`/bookings?production=${production}`)
    .then(({ data }) => dispatch({
      type: GET_BOOKINGS_SUCCEEDED,
      payload: { data },
    }))
    .catch(error => dispatch({
      type: GET_BOOKINGS_FAILED,
      payload: { error },
    }));
};

export const getBooking = ({ _id }) => (dispatch) => {
  get(`/bookings/${_id}`)
    .then(({ data }) => dispatch({
      type: GET_BOOKING_SUCCEEDED,
      payload: { data },
    }))
    .catch(error => dispatch({
      type: GET_BOOKING_FAILED,
      payload: { error },
    }));
};

export const clearBooking = () => ({
  type: CLEAR_BOOKING,
});

export const filterBookings = ({ field, value }) => (dispatch, getState) => {
  const bookings = getState().getIn(['bookings', 'browse', 'bookings']).toJS();
  const filter = new Fuse(bookings, {
    shouldSort: true,
    threshold: 0.3,
    keys: [field],
  });
  dispatch({
    type: FILTER_BOOKINGS,
    payload: {
      filtered: filter.search(value),
    },
  });
};
