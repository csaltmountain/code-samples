import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import auth from '../../auth';
import Browse from '../containers/Browse';
import Read from '../containers/Read';

function Bookings() {
  return (
    <div className="container">
      <Helmet>
        <title>{'Bookings — Salt Mountain Box Office'}</title>
      </Helmet>
      <h1 className="text-center mb-5">Bookings</h1>
      <Switch>
        <Route path="/bookings/:_id" component={Read} />
        <Route path="/bookings" component={Browse} />
      </Switch>
    </div>
  );
}

Bookings.propTypes = {};

Bookings.defaultProps = {};

export default auth(Bookings);
