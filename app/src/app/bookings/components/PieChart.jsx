import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'chart.js';

class PieChart extends React.Component {
  componentDidMount() {
    this.drawChart();
  }

  componentDidUpdate() {
    this.drawChart();
  }

  drawChart() {
    this.chart = new Chart(this.canvas, {
      type: 'doughnut',
      data: {
        labels: ['Online', 'Telephone', 'In-person'],
        datasets: [
          {
            data: this.props.data,
            backgroundColor: [
              'rgb(255, 99, 132)',
              'rgb(54, 162, 235)',
              'rgb(255, 206, 86)',
            ],
          },
        ],
      },
      options: {
        title: {
          display: true,
          text: 'Booking Method',
          fontFamily: 'Ubuntu',
          fontSize: 20,
        },
      },
    });
  }

  render() {
    return (
      <canvas height="400" ref={(canvas) => { this.canvas = canvas; }} />
    );
  }
}

PieChart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.number),
};

PieChart.defaultProps = {
  data: [],
};

export default PieChart;
