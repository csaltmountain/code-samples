import React from 'react';
import PropTypes from 'prop-types';
import dropRight from 'lodash/dropRight';

import PieChart from './PieChart';

function Statistics({ statistics }) {
  return (
    <div>
      <PieChart data={dropRight(statistics.map(item => item.bookings))} />
      <div className="list-group">
        {statistics.map(item => (
          <div key={item._id} className="list-group-item">
            <div className="h3 d-flex justify-content-between">
              <div>{item._id}</div>
            </div>
            <div className="d-flex justify-content-between">
              <div>Bookings:</div>
              <div>{item.bookings}</div>
            </div>
            <div className="d-flex justify-content-between">
              <div>Tickets:</div>
              <div>{item.tickets}</div>
            </div>
            <div className="d-flex justify-content-between">
              <div>Revenue:</div>
              <div>£{item.revenue}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

Statistics.propTypes = {
  statistics: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Statistics;
