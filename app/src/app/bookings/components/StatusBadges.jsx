import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

function StatusBadges({ reservation, platform, status, createdAt }) {
  const date = moment(createdAt).format('ddd DD MMM YY HH:mm');

  const statusBadge = (badge, icon) => (
    <span className={`badge badge-${badge}`}><i className={`fa fa-fw fa-${icon}`} /></span>
  );

  const platformBadge = icon => (
    <span className="badge badge-info"><i className={`fa fa-fw fa-${icon}`} /> {date}</span>
  );

  return (
    <div>
      {reservation ? (
        <div className="badge badge-danger">
          <i className="fa fa-fw fa-pause" /> Reservation
        </div>
      ) : (
        <div>
          {status === 'complete' && statusBadge('success', 'check')}
          {status === 'pending' && statusBadge('warning', 'warning')}
          {status === 'failed' && statusBadge('primary', 'exclamation-circle')}
          {' '}
          {platform === 'online' && platformBadge('desktop')}
          {platform === 'telephone' && platformBadge('phone')}
          {platform === 'terminal' && platformBadge('user')}
        </div>
      )}
    </div>
  );
}

StatusBadges.propTypes = {
  reservation: PropTypes.bool,
  platform: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
};

StatusBadges.defaultProps = {
  reservation: false,
};

export default StatusBadges;
