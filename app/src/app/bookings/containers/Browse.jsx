import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { List } from 'immutable';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { getBookings, filterBookings } from '../actionCreators';

import ProductionFilter from '../../production-filter';
import Statistics from '../components/Statistics';
import StatusBadges from '../components/StatusBadges';

function Bookings({ bookings, statistics, filtered, dispatchGetBookings, dispatchFilterBookings }) {
  const list = filtered.size > 0 ? filtered.toJS() : bookings.toJS();

  return (
    <div>
      <ProductionFilter
        onSelect={production => dispatchGetBookings(production)}
      />
      {bookings.size === 0 ? (
        <div className="text-center">
          <i className="fa fa-spinner fa-pulse fa-4x fa-fw" />
        </div>
      ) : (
        <div className="row">
          <div className="col order-lg-1 mb-4">
            <div className="row">
              <div className="col">
                <div className="form-group">
                  <label htmlFor="search-by-name">Search By Name</label>
                  <input
                    id="search-by-name"
                    className="form-control"
                    placeholder="e.g. John Smith"
                    onChange={event => dispatchFilterBookings('name', event.target.value)}
                  />
                </div>
              </div>
              <div className="col">
                <div className="form-group">
                  <label htmlFor="search-by-name">Search By Reference</label>
                  <input
                    id="search-by-name"
                    className="form-control"
                    placeholder="e.g. 1718A001"
                    onChange={event => dispatchFilterBookings('reference', event.target.value)}
                  />
                </div>
              </div>
            </div>
            <div className="list-group">
              {list.map((booking) => {
                const performance = moment.utc(booking.performance).format('ddd DD MMM YY HH:mm');
                const isToday = moment().isSame(booking.performance, 'day');
                const body = (
                  <div>
                    <Link to={`/bookings/${booking._id}`} className="h4 cart-title">{booking.name}</Link>
                    <div className={`${isToday ? 'text-success' : 'text-muted'} card-subtitle mt-2`}>
                      <div>{performance}</div>
                      <div>{booking.seatsString}</div>
                      <div>{`Full: ${booking.tickets.full} | Family: ${booking.tickets.family} | Concessions: ${booking.tickets.concessions}`}</div>
                    </div>
                  </div>
                );

                return (
                  <div key={booking._id} className="list-group-item text-center text-sm-left">
                    <div className="d-sm-flex align-items-center justify-content-between">
                      {booking.seatsReserved ? (
                        body
                      ) : (
                        <del>{body}</del>
                      )}
                      <div className="text-sm-right">
                        <StatusBadges {...booking} />
                        {booking.seatsReserved && (
                          <a href={`/api/bookings/${booking._id}/tickets`} target="_blank" className="mt-2 btn btn-secondary">
                            <i className="fa fa-print" /> Print Tickets
                          </a>
                        )}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="col-lg-4 mb-4">
            <Statistics statistics={statistics.toJS()} />
          </div>
        </div>
      )}
    </div>
  );
}

Bookings.propTypes = {
  bookings: ImmutablePropTypes.list,
  statistics: ImmutablePropTypes.list,
  filtered: ImmutablePropTypes.list,
  dispatchGetBookings: PropTypes.func.isRequired,
  dispatchFilterBookings: PropTypes.func.isRequired,
};

Bookings.defaultProps = {
  bookings: List(),
  statistics: List(),
  filtered: List(),
};

export default connect(
  state => ({
    bookings: state.getIn(['bookings', 'browse', 'bookings']),
    statistics: state.getIn(['bookings', 'browse', 'statistics']),
    filtered: state.getIn(['bookings', 'browse', 'filtered']),
  }),
  dispatch => ({
    dispatchGetBookings: production => dispatch(getBookings(production)),
    dispatchFilterBookings: (field, value) => dispatch(filterBookings({ field, value })),
  }),
)(Bookings);
