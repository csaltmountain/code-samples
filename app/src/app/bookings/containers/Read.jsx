import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Map } from 'immutable';
import ImmutablePropTypes from 'react-immutable-proptypes';
import smoothscroll from 'smoothscroll';
import get from 'lodash/get';
import moment from 'moment';

import { getBooking, clearBooking } from '../actionCreators';

import StatusBadges from '../components/StatusBadges';

class Read extends React.Component {
  componentWillMount() {
    smoothscroll(0);
    this.props.dispatchGetBooking();
  }

  componentWillUnmount() {
    this.props.dispatchClearBooking();
  }

  render() {
    const {
      _id,
      reference,
      name,
      telephone,
      production,
      performance,
      seatsString,
      tickets,
      email,
      emails,
      charge,
      chargeError,
      platform,
      status,
      reservation,
      seatsReserved,
      createdAt,
      total,
    } = this.props.booking.toJS();

    if (this.props.booking.size === 0) {
      return (
        <div className="text-center">
          <i className="fa fa-spinner fa-pulse fa-4x fa-fw" />
        </div>
      );
    }

    return (
      <div className="container mb-5">
        <div className="d-flex justify-content-between mb-3">
          <Link to="/bookings" className="btn btn-primary">
            <i className="fa fa-chevron-left" /> Back
          </Link>
          {seatsReserved && (
            <a href={`/api/bookings/${_id}/tickets`} target="_blank" className="btn btn-secondary">
              <i className="fa fa-ticket" /> View / Print Tickets
            </a>
          )}
        </div>
        <div className="row mb-3">
          <div className="col-sm mb-3">
            <div className="card">
              <div className="card-body">
                <dl className="mb-0">
                  <dt className="d-flex justify-content-between">
                    <div>Reference</div>
                    <StatusBadges
                      platform={platform}
                      status={status}
                      reservation={reservation}
                      createdAt={createdAt}
                    />
                  </dt>
                  <dd>{reference}</dd>
                  <dt>Name</dt>
                  <dd>{name}</dd>
                  <dt>Telephone</dt>
                  <dd className="mb-0">{telephone}</dd>
                </dl>
              </div>
            </div>
          </div>
          <div className="col-sm">
            <div className="card">
              <div className="card-body">
                <dl className="mb-0">
                  <dt>Production</dt>
                  <dd>{get(production, 'shortTitle') || get(production, 'title')}</dd>
                  <dt>Performance</dt>
                  <dd>{moment.utc(performance).format('dddd Do MMMM YYYY [at] HH:mm')}</dd>
                  <dt>Seats</dt>
                  <dd>{seatsReserved ? seatsString : (<del>{seatsString}</del>)}</dd>
                  <dt>Tickets</dt>
                  <dd>
                    <div>{`Full Price: ${get(tickets, 'full')}`}</div>
                    <div>{`Family: ${get(tickets, 'family')}`}</div>
                    <div>{`Concessions: ${get(tickets, 'concessions')}`}</div>
                  </dd>
                  <dt>Total</dt>
                  <dd className="mb-0">£{total}</dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
        {(charge || chargeError) && (
          <div className="card mb-3">
            <div className={chargeError ? 'card-header bg-warning' : 'card-header'}>
              Charge
            </div>
            <div className="card-body">
              <pre className="mb-0">{JSON.stringify(chargeError || charge, null, 2)}</pre>
            </div>
          </div>
        )}
        {email && (
          <div className="card">
            <div className="card-header">
              <div>Emails</div>
            </div>
            <div className="card-body">
              <pre className="mb-0">{JSON.stringify(emails, null, 2)}</pre>
            </div>
          </div>
        )}
      </div>
    );
  }
}

Read.propTypes = {
  booking: ImmutablePropTypes.map,
  dispatchGetBooking: PropTypes.func.isRequired,
  dispatchClearBooking: PropTypes.func.isRequired,
};

Read.defaultProps = {
  booking: Map(),
};

export default connect(
  state => ({
    booking: state.getIn(['bookings', 'read']),
  }),
  (dispatch, ownProps) => ({
    dispatchGetBooking: () => dispatch(getBooking({ _id: ownProps.match.params._id })),
    dispatchClearBooking: () => dispatch(clearBooking()),
  }),
)(Read);
