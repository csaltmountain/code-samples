import Bookings from './components/Bookings';
import reducer from './reducer';

export { Bookings as default, reducer };
