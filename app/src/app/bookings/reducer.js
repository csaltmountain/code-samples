import { Map, fromJS } from 'immutable';
import {
  GET_BOOKINGS_SUCCEEDED,
  GET_BOOKING_SUCCEEDED,
  CLEAR_BOOKING,
  FILTER_BOOKINGS,
} from './actions';

export default (state = Map(), { type, payload }) => {
  switch (type) {
    case GET_BOOKINGS_SUCCEEDED: {
      return state.set('browse', fromJS(payload.data));
    }

    case GET_BOOKING_SUCCEEDED: {
      return state.set('read', fromJS(payload.data));
    }

    case CLEAR_BOOKING: {
      return state.delete('read');
    }

    case FILTER_BOOKINGS: {
      return state.setIn(['browse', 'filtered'], fromJS(payload.filtered));
    }

    default: {
      return state;
    }
  }
};
