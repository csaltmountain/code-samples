import axios from 'axios';
import get from 'lodash/get';
import set from 'lodash/set';
import sequenceSeats from './utils';
import { seatsValid, getCheckoutTotal, getSeats } from './selectors';

import {
  HYDRATE_META,
  CHOOSE_PRODUCTION,
  CHOOSE_PERFORMANCE,
  TOGGLE_SEAT,
  SEQUENCE_SEATS,
  UPDATE_INPUT,
  NAVIGATE,
  SUBMITTING_FORM,
  SUBMITTING_FORM_SUCCEEDED,
  SUBMITTING_FORM_FAILED,
  SUBMITTING_FORM_CONFLICTED,
  RESET_FORM,
} from './actions';

export const hydrateMeta = payload => ({
  type: HYDRATE_META,
  payload,
});

export const updateInput = payload => ({
  type: UPDATE_INPUT,
  payload,
});

export const navigate = payload => ({
  type: NAVIGATE,
  payload,
});

export const chooseProduction = payload => (dispatch) => {
  dispatch({
    type: CHOOSE_PRODUCTION,
    payload,
  });
};

export const choosePerformance = payload => ({
  type: CHOOSE_PERFORMANCE,
  payload,
});

export const toggleSeat = ({ rowIndex, seatIndex }) => (dispatch, getState) => {
  const state = getState();
  const { status, row, seat } = state
    .getIn(['form', 'meta', 'availability'])
    .getIn([rowIndex, seatIndex])
    .toJS();
  const limitReached = seatsValid(state);

  const newStatus = status === 'available' && !limitReached ? 'selected' : 'available';

  if (!limitReached || status === 'selected') {
    dispatch({
      type: TOGGLE_SEAT,
      payload: { row, seat, rowIndex, seatIndex, newStatus },
    });

    const newState = getState();
    if (seatsValid(newState)) {
      dispatch({
        type: SEQUENCE_SEATS,
        payload: sequenceSeats(
          newState.getIn(['form', 'meta', 'availability']),
          newState.getIn(['form', 'data', 'seats']),
        ),
      });
    }
  }
};

export const submitForm = stripe => (dispatch, getState) => {
  const state = getState();
  const formData = state.getIn(['form', 'data']).toJS();

  dispatch({
    type: SUBMITTING_FORM,
  });

  const admin = state.getIn(['auth', 'admin']);
  const url = admin ? '/admin/bookings' : '/bookings';

  if (admin) {
    set(formData, 'admin.user', admin);
  }

  const submit = token =>
    axios
      .post(url, {
        ...formData,
        cardPayment: !token ? false : formData.cardPayment,
        seatsString: getSeats(state),
        total: getCheckoutTotal(state),
        token,
        tickets: {
          full: 0,
          family: 0,
          concessions: 0,
          ...formData.tickets,
        },
      })
      .then(({ data }) => {
        dispatch({
          type: SUBMITTING_FORM_SUCCEEDED,
          payload: { data },
        });
      })
      .catch((err) => {
        if (err.response.status === 409) {
          return dispatch({
            type: SUBMITTING_FORM_CONFLICTED,
          });
        }
        return dispatch({
          type: SUBMITTING_FORM_FAILED,
          payload: {
            message: get(err, 'response.data.message'),
          },
        });
      });

  if (
    admin === 'terminal' ||
    (admin === 'telephone' && formData.admin && formData.admin.reservation) ||
    getCheckoutTotal(state) === 0
  ) {
    dispatch({
      type: SUBMITTING_FORM,
    });
    return submit();
  }

  if (formData.cardPayment) {
    return stripe.createToken().then((result) => {
      if (result.err) {
        dispatch({
          type: SUBMITTING_FORM_FAILED,
        });
      }
      return submit(result.token.id);
    });
  }

  return dispatch({
    type: SUBMITTING_FORM_FAILED,
    payload: {
      message: 'Please complete all the form fields',
    },
  });
};

export const resetForm = () => ({
  type: RESET_FORM,
});
