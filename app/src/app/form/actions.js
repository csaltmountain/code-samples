export const HYDRATE_META = 'HYDRATE_META';

export const NAVIGATE = 'NAVIGATE';

export const CHOOSE_PRODUCTION = 'CHOOSE_PRODUCTION';
export const CHOOSE_PERFORMANCE = 'CHOOSE_PERFORMANCE';

export const TOGGLE_SEAT = 'TOGGLE_SEAT';
export const SEQUENCE_SEATS = 'SEQUENCE_SEATS';

export const UPDATE_INPUT = 'UPDATE_INPUT';

export const SUBMITTING_FORM = 'SUBMITTING_FORM';
export const SUBMITTING_FORM_SUCCEEDED = 'SUBMITTING_FORM_SUCCEEDED';
export const SUBMITTING_FORM_FAILED = 'SUBMITTING_FORM_FAILED';
export const SUBMITTING_FORM_CONFLICTED = 'SUBMITTING_FORM_CONFLICTED';

export const RESET_FORM = 'RESET_FORM';
