import React from 'react';
import { Elements } from 'react-stripe-elements';

import CheckoutForm from '../containers/CheckoutForm';

function Checkout() {
  return (
    <Elements>
      <CheckoutForm />
    </Elements>
  );
}

export default Checkout;
