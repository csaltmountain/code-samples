import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import numeral from 'numeral';

function ReviewSidebar({
  shortTitle,
  title,
  performance,
  tickets,
  seats,
  total,
  complete,
}) {
  return (
    <div className="card mb-3">
      <div className="card-body">
        <h4 className="card-title">{complete ? 'Complete' : 'Review'}</h4>
        <dl className="mb-0">
          <dt>Production</dt>
          <dd>{shortTitle || title}</dd>
          <dt>Performance</dt>
          <dd>{moment.utc(performance).format('dddd Do MMMM YYYY [at] HH:mm')}</dd>
          <dt>Tickets</dt>
          <dd>
            {tickets.full && <div>Full Price: {tickets.full}</div>}
            {tickets.family && <div>Family: {tickets.family}</div>}
            {tickets.concessions && <div>Concessions: {tickets.concessions}</div>}
          </dd>
          <dt>Seats</dt>
          <dd>{seats}</dd>
          <dt>{complete ? 'Amount Paid' : 'Amount to Pay'}</dt>
          <dd className="mb-0">£{numeral(total).format('0.00')}</dd>
        </dl>
      </div>
    </div>
  );
}

ReviewSidebar.propTypes = {
  shortTitle: PropTypes.string,
  title: PropTypes.string,
  performance: PropTypes.string,
  tickets: PropTypes.objectOf(PropTypes.number),
  seats: PropTypes.string,
  total: PropTypes.number,
  complete: PropTypes.bool,
};

ReviewSidebar.defaultProps = {
  shortTitle: '',
  title: '',
  performance: '',
  tickets: {},
  seats: '',
  total: 0,
  complete: false,
};

export default ReviewSidebar;
