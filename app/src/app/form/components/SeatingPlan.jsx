import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import uuid from 'uuid/v4';

const indexStyle = { width: `${100 / 28}%` };
const seatStyle = { width: `${100 / 14}%` };

function SeatingPlan({ availability, handleClick }) {
  return (
    <div className="seating-plan">
      {handleClick && (
        <div className="alert alert-dark text-center">
          <strong>STAGE</strong>
        </div>
      )}
      <div className="d-flex align-items-center">
        <div style={indexStyle} />
        {Array(13).fill(null).map((item, index) => (
          <div key={uuid()} className="text-center" style={seatStyle}>{13 - index}</div>
        ))}
      </div>
      {availability.map((row, rowIndex) => (
        <div key={row[0].row} className="seating-plan__row d-flex align-items-center">
          <div className="text-center" style={indexStyle}>{row[0].row}</div>
          {row.map((item, seatIndex) => {
            let element = 'div';

            const props = {
              key: item.seat,
              className: `btn d-block seat seat--${item.status}`,
              style: seatStyle,
              disabled: !handleClick || item.status === 'booked',
            };

            if (handleClick) {
              element = 'button';
              props.onClick = () => {
                handleClick(rowIndex, seatIndex);
              };
            } else if (item.status === 'booked') {
              element = Link;
              props.to = `/bookings/${item._id}`;
            }

            return React.createElement(
              element,
              props,
              <span>
                <i className={`fa ${item.status === 'available' ? 'fa-user-o' : 'fa-user'}`} />
                {!handleClick && item.status === 'booked' && (
                  <div className="tooltip p-1 text-center">
                    {item.name}
                    <br />
                    <small>{item.reference}</small>
                  </div>
                )}
              </span>,
            );
          })}
          <div className="text-center" style={indexStyle}>{row[0].row}</div>
        </div>
      ))}
      {handleClick && (
        <div className="d-flex mt-3 justify-content-around">
          <div>
            <i className="fa fa-user seat--booked" /> Booked
          </div>
          <div>
            <i className="fa fa-user-o text-primary" /> Available
          </div>
          <div>
            <i className="fa fa-user text-primary" /> Selected
          </div>
        </div>
      )}
    </div>
  );
}

SeatingPlan.propTypes = {
  availability: PropTypes.arrayOf(PropTypes.array).isRequired,
  handleClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

SeatingPlan.defaultProps = {
  handleClick: false,
};

export default SeatingPlan;
