import React from 'react';
import PropTypes from 'prop-types';
import numeral from 'numeral';

function TicketInput({
  title,
  subtitle,
  path,
  count,
  price,
  dispatchUpdateInput,
}) {
  return (
    <div className="card text-center">
      <div className="card-body">
        <h4 className="card-title">{title}
          <div className="text-muted">
            {subtitle ? <small>{subtitle}</small> : <span>&nbsp;</span>}
          </div>
        </h4>
        <div className="row">
          <button
            className="col-4 btn btn-link"
            onClick={() => dispatchUpdateInput(['tickets', path], count - 1)}
            disabled={count === 0}
          >
            <i className="fa fa-fw fa-2x fa-chevron-down" />
          </button>
          <div className="col-4 display-1">{count}</div>
          <button
            className="col-4 btn btn-link"
            onClick={() => dispatchUpdateInput(['tickets', path], count + 1)}
          >
            <i className="fa fa-fw fa-2x fa-chevron-up" />
          </button>
        </div>
        <p className="card-text lead mt-2">£{numeral(price).format('0')}</p>
      </div>
    </div>
  );
}

TicketInput.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  path: PropTypes.string.isRequired,
  count: PropTypes.number,
  price: PropTypes.number.isRequired,
  dispatchUpdateInput: PropTypes.func.isRequired,
};

TicketInput.defaultProps = {
  subtitle: '',
  count: 0,
};

export default TicketInput;
