import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateInput } from '../actionCreators';

function AdminForm({ adminNoEmail, adminReservation, adminNotes, dispatchUpdateInput }) {
  return (
    <div className="card mb-3">
      <div className="card-header text-white bg-danger">
        Admin Use Only
      </div>
      <div className="card-body">
        <div className="form-group">
          <div className="form-check mb-0">
            <label htmlFor="admin-no-email" className="form-check-label">
              <input
                id="admin-no-email"
                type="checkbox"
                className="form-check-input"
                onChange={(event) => {
                  dispatchUpdateInput(['admin', 'noEmail'], event.target.checked);
                  dispatchUpdateInput(['email'], undefined);
                }}
                checked={adminNoEmail}
              /> {'No Email'}
            </label>
          </div>
          <div className="form-check mb-0">
            <label htmlFor="admin-reservation" className="form-check-label">
              <input
                id="admin-reservation"
                type="checkbox"
                className="form-check-input"
                onChange={event => dispatchUpdateInput(['admin', 'reservation'], event.target.checked)}
                checked={adminReservation}
              /> {'Reservation'}
            </label>
          </div>
        </div>
        <div className="form-group">
          <label htmlFor="admin-notes" className="required">Notes</label>
          <textarea
            id="admin-notes"
            className="form-control"
            value={adminNotes}
            onChange={event => dispatchUpdateInput(['admin', 'notes'], event.target.value)}
            rows="8"
          />
        </div>
      </div>
    </div>
  );
}

AdminForm.propTypes = {
  adminNoEmail: PropTypes.bool,
  adminReservation: PropTypes.bool,
  adminNotes: PropTypes.string,
  dispatchUpdateInput: PropTypes.func.isRequired,
};

AdminForm.defaultProps = {
  adminNoEmail: false,
  adminReservation: false,
  adminNotes: '',
  dispatchUpdateInput: '',
};

export default connect(
  state => ({
    adminNoEmail: state.getIn(['form', 'data', 'admin', 'noEmail']),
    adminReservation: state.getIn(['form', 'data', 'admin', 'reservation']),
    adminNotes: state.getIn(['form', 'data', 'admin', 'notes']),
  }),
  dispatch => ({
    dispatchUpdateInput: (path, value) => dispatch(updateInput({ path, value })),
  }),
)(AdminForm);
