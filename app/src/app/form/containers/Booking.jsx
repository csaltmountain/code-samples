import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import ReactGA from 'react-ga';
import smoothscroll from 'smoothscroll';
import { Helmet } from 'react-helmet';

import Productions from './Productions';
import Performances from './Performances';
import Tickets from './Tickets';
import Seats from './Seats';
import Checkout from '../components/Checkout';
import Complete from './Complete';

import { Logo } from '../../elements';

import { navigate } from '../actionCreators';
import { ticketsValid, seatsValid } from '../selectors';

import '../styles/header.scss';
import '../styles/booking.scss';

const components = {
  productions: Productions,
  performances: Performances,
  tickets: Tickets,
  seats: Seats,
  checkout: Checkout,
  complete: Complete,
};

class Booking extends React.Component {
  componentWillMount() {
    if (process.env.NODE_ENV === 'production') {
      ReactGA.event({
        category: 'Booking',
        action: this.props.stage,
      });
    }
  }

  componentWillUpdate(nextProps) {
    if (this.props.stage !== nextProps.stage) {
      smoothscroll(0);
      if (process.env.NODE_ENV === 'production') {
        ReactGA.event({
          category: 'Booking',
          action: nextProps.stage,
        });
      }
    }
  }

  render() {
    const {
      stage,
      stages,
      isValid,
      hideButtons,
      dispatchNavigate,
      admin,
    } = this.props;
    const button = (icon, path) => (
      <button
        className="btn btn-link p-0"
        onClick={() => dispatchNavigate(path)}
        disabled={icon === 'right' && !isValid}
      >
        {icon === 'right' && <div className={isValid ? 'tada mr-1 d-inline-block' : 'mr-1 d-inline-block'}>Next</div>}
        <i className={`fa fa-2x fa-chevron-${icon} align-middle`} />
        {icon === 'left' && <span className="ml-1">Back</span>}
      </button>
    );

    const back = stages.get('back');
    const next = stages.get('next');

    return (
      <div className="container pb-4">
        <Helmet>
          <title>{'Salt Mountain Box Office'}</title>
        </Helmet>

        {!admin && (
          <div className={stage === 'productions' ? 'header' : 'header header--compact'}>
            <Logo />
            <h1>Salt Mountain</h1>
            <h2>Box Office</h2>
          </div>
        )}

        {stage !== 'productions' && (
          <div>
            <div className="row align-items-center justify-content-between mb-4">
              <div className="col-12 col-sm-7 order-sm-2 text-center mb-3 mb-sm-0">
                <h3>{stages.get('title')}<br /><small className="text-muted">{stages.get('subtitle')}</small></h3>
              </div>
              <div className="col order-sm-1">
                {!hideButtons && back && button('left', back)}
              </div>
              <div className="col order-sm-3 text-right">
                {!hideButtons && next && button('right', next)}
              </div>
            </div>
            <div className="progress mb-4">
              <div className="progress-bar" role="progressbar" style={{ width: `${stages.get('percentage')}%` }} />
            </div>
          </div>
        )}

        {React.createElement(components[stage])}

        {!hideButtons && (
          <div className="d-flex align-items-center justify-content-between mt-4">
            {back && button('left', stages.get('back'))}
            {next && button('right', stages.get('next'))}
          </div>
        )}
      </div>
    );
  }
}

Booking.propTypes = {
  stage: PropTypes.string.isRequired,
  stages: ImmutablePropTypes.map.isRequired,
  isValid: PropTypes.bool,
  hideButtons: PropTypes.bool,
  dispatchNavigate: PropTypes.func.isRequired,
  admin: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
};

Booking.defaultProps = {
  hideButtons: false,
  isValid: false,
};

const mapStateToProps = (state) => {
  const validators = {
    tickets: ticketsValid,
    seats: seatsValid,
  };

  const stage = state.getIn(['form', 'ui', 'stage']);
  return {
    stage,
    stages: state.getIn(['form', 'ui', 'stages', stage]),
    isValid: validators[stage] ? validators[stage](state) : false,
    admin: state.getIn(['auth', 'admin']),
  };
};

const mapDispatchToProps = dispatch => ({
  dispatchNavigate: path => dispatch(navigate({ path })),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Booking);
