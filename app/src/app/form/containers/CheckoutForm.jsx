/* eslint react/forbid-prop-types: "off" */
import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { injectStripe, CardElement } from 'react-stripe-elements';
import { PhoneNumberUtil, PhoneNumberFormat } from 'google-libphonenumber';

import { navigate, updateInput, submitForm } from '../actionCreators';
import { getCheckoutTotal, getSeats } from '../selectors';

import ReviewSidebar from '../components/ReviewSidebar';
import AdminForm from './AdminForm';

function CheckoutForm({
  admin,
  title,
  shortTitle,
  performance,
  tickets,
  seats,
  name,
  telephone,
  email,
  total,
  submittingForm,
  dispatchUpdateInput,
  dispatchSubmitForm,
  dispatchNavigate,
  submittingFormFailed,
  submittingFormFailedMessage,
  submittingFormConflicted,
  stripe,
  adminNoEmail,
  adminReservation,
}) {
  return (
    <div>
      {submittingFormConflicted ? (
        <div className="alert alert-info text-center">
          <div className="mb-2"><strong>{'Oh no, you\'ve been pipped to the post!'}</strong></div>
          <div className="mb-3">{'Whilst you were filling out this form, a booking has been placed with some of the same seats you\'ve selected. You need to go back and rechoose different seats.'}</div>
          <button
            className="btn btn-info"
            onClick={() => dispatchNavigate('seats')}
          >Rechoose Seats</button>
        </div>
      ) : (
        <form
          className="checkout"
          autoComplete="off"
          noValidate
          onSubmit={(event) => {
            event.preventDefault();
            event.stopPropagation();
            event.target.classList.add('was-validated');
            if (event.target.checkValidity()) {
              dispatchSubmitForm(stripe);
            }
          }}
        >
          <div className="row">
            <div className="col-lg-4">
              <ReviewSidebar
                shortTitle={shortTitle}
                title={title}
                tickets={{ full: tickets.get('full'), family: tickets.get('family'), concessions: tickets.get('concessions') }}
                performance={performance}
                seats={seats}
                total={total}
              />
            </div>
            <div className="col">
              <div className="card mb-3">
                <div className="card-body">
                  <h4 className="card-title">
                    Your Details
                  </h4>
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input
                      id="name"
                      className="form-control"
                      type="text"
                      placeholder="e.g. John Smith"
                      value={name}
                      onChange={event =>
                        dispatchUpdateInput({
                          target: {
                            id: 'name',
                            value: event.target.value.replace(/\b\w+/g, match => match.charAt(0).toUpperCase() + match.slice(1)).replace(/'/g, () => '’'),
                          },
                        })
                      }
                      required
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="telephone" className="required">Telephone</label>
                    <input
                      id="telephone"
                      className="form-control"
                      type="tel"
                      placeholder="e.g. 07000 000000"
                      value={telephone}
                      onChange={dispatchUpdateInput}
                      pattern="^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?#(\d{4}|\d{3}))?$"
                      onBlur={(event) => {
                        if (event.target.value) {
                          try {
                            const phoneUtil = PhoneNumberUtil.getInstance();
                            const phoneNumber = phoneUtil.parse(event.target.value, 'GB');
                            dispatchUpdateInput({ target: { id: 'telephone', value: phoneUtil.format(phoneNumber, PhoneNumberFormat.NATIONAL) } });
                          } catch (e) {
                            console.log(e);
                          }
                        }
                      }}
                      required
                    />
                    <small className="form-text text-muted">{'Please enter a valid mobile or landline number.'}</small>
                  </div>
                  {admin !== 'terminal' && !adminNoEmail && !adminReservation && (
                    <div className="form-group">
                      <label htmlFor="email" className="required">Email Address</label>
                      <input
                        id="email"
                        className="form-control"
                        type="email"
                        placeholder="e.g. john@example.com"
                        value={email}
                        onChange={dispatchUpdateInput}
                        required
                      />
                      <small className="form-text text-muted">{'Please enter a valid email. We will send your tickets and a payment receipt.'}</small>
                    </div>
                  )}
                  <small><i>{'We\'ll never share your details with anyone else.'}</i></small>
                </div>
              </div>
              {total !== 0 && admin === 'telephone' && (
                <AdminForm />
              )}
              {total !== 0 && admin !== 'terminal' && !adminReservation && (
                <div className="card mb-3">
                  <div className="card-body">
                    <h4 className="card-title">Credit/Debit Card</h4>
                    <CardElement
                      onChange={event => dispatchUpdateInput({ target: { id: 'cardPayment', value: event.complete } })}
                      style={{ base: { fontSize: '17px' } }}
                    />
                    <div className="mt-4 text-right text-muted">
                      <i className="fa fa-fw fa-2x fa-cc-visa" />{' '}
                      <i className="fa fa-fw fa-2x fa-cc-mastercard" />{' '}
                      <i className="fa fa-fw fa-2x fa-cc-amex" />
                    </div>
                  </div>
                </div>
              )}
              <div className="text-center">
                <button
                  className="btn btn-primary btn-lg"
                  disabled={submittingForm}
                >
                  {submittingForm ? (
                    <span><i className="fa fa-cog fa-spin fa-fw" /> Processing</span>
                  ) : (
                    <span>Complete Booking</span>
                  )}
                </button>
              </div>
              {submittingFormFailed &&
                <div className="alert alert-warning mt-3 text-center">
                  {submittingFormFailedMessage}
                </div>
              }
            </div>
          </div>
        </form>
      )}
    </div>
  );
}

CheckoutForm.propTypes = {
  admin: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  title: PropTypes.string.isRequired,
  shortTitle: PropTypes.string,
  performance: PropTypes.string.isRequired,
  tickets: ImmutablePropTypes.map.isRequired,
  seats: PropTypes.string,
  name: PropTypes.string,
  adminNoEmail: PropTypes.bool,
  adminReservation: PropTypes.bool,
  email: PropTypes.string,
  telephone: PropTypes.string,
  total: PropTypes.number.isRequired,
  submittingForm: PropTypes.bool.isRequired,
  dispatchUpdateInput: PropTypes.func.isRequired,
  dispatchSubmitForm: PropTypes.func.isRequired,
  dispatchNavigate: PropTypes.func.isRequired,
  submittingFormFailed: PropTypes.bool,
  submittingFormFailedMessage: PropTypes.string,
  submittingFormConflicted: PropTypes.bool,
  stripe: PropTypes.object.isRequired,
};

CheckoutForm.defaultProps = {
  shortTitle: '',
  tickets: {},
  seats: [],
  name: '',
  adminNoEmail: false,
  adminReservation: false,
  adminNotes: 'false',
  email: '',
  telephone: '',
  cardPayment: false,
  submittingFormFailed: false,
  submittingFormFailedMessage: 'There was an error.',
  submittingFormConflicted: false,
};

const mapStateToProps = state => ({
  admin: state.getIn(['auth', 'admin']),
  title: state.getIn(['form', 'meta', 'production', 'title']),
  shortTitle: state.getIn(['form', 'meta', 'production', 'shortTitle']),
  performance: state.getIn(['form', 'data', 'performance']),
  tickets: state.getIn(['form', 'data', 'tickets']),
  name: state.getIn(['form', 'data', 'name']),
  email: state.getIn(['form', 'data', 'email']),
  telephone: state.getIn(['form', 'data', 'telephone']),
  seats: getSeats(state),
  total: getCheckoutTotal(state),
  submittingForm: state.getIn(['form', 'ui', 'submittingForm']),
  submittingFormFailed: state.getIn(['form', 'ui', 'submittingFormFailed']),
  submittingFormFailedMessage: state.getIn(['form', 'ui', 'submittingFormFailedMessage']),
  submittingFormConflicted: state.getIn(['form', 'ui', 'submittingFormConflicted']),
  adminNoEmail: state.getIn(['form', 'data', 'admin', 'noEmail']),
  adminReservation: state.getIn(['form', 'data', 'admin', 'reservation']),
  adminNotes: state.getIn(['form', 'data', 'admin', 'notes']),
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateInput: event =>
    dispatch(updateInput({
      path: [event.target.id],
      value: event.target.value,
    })),
  dispatchSubmitForm: stripe => dispatch(submitForm(stripe)),
  dispatchNavigate: path => dispatch(navigate({ path })),
});

export default compose(
  injectStripe,
  connect(mapStateToProps, mapDispatchToProps),
)(CheckoutForm);
