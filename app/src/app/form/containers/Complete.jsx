import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Map } from 'immutable';
import { connect } from 'react-redux';
import { resetForm } from '../actionCreators';

import ReviewSidebar from '../components/ReviewSidebar';

function Complete({ admin, complete, dispatchResetForm, ...passThroughProps }) {
  const { _id, reference, email, seatsString, ...passThrough } = complete.toJS();

  return (
    <div className="row">
      <div className="col-lg-4">
        <ReviewSidebar {...passThrough} {...passThroughProps} seats={seatsString} complete />
      </div>
      <div className="col">
        <div className="text-center">
          <h2>Booking Reference: {reference}</h2>
          <button onClick={dispatchResetForm} className="btn btn-link">
            Start Again
          </button>
          {admin && (
            <div>
              <a
                href={`https://admin.saltmountainhq.com/bookings/${_id}/tickets`}
                target="_blank"
                className="btn btn-danger mt-5"
              >
                Print Tickets
              </a>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

Complete.propTypes = {
  admin: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  complete: ImmutablePropTypes.map,
  dispatchResetForm: PropTypes.func.isRequired,
};

Complete.defaultProps = {
  complete: Map(),
};

const mapStateToProps = state => ({
  admin: state.getIn(['auth', 'admin']),
  complete: state.getIn(['form', 'complete']),
  title: state.getIn(['form', 'meta', 'production', 'title']),
  shortTitle: state.getIn(['form', 'meta', 'production', 'shortTitle']),
});

const mapDispatchToProps = dispatch => ({
  dispatchResetForm: () => dispatch(resetForm()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Complete);
