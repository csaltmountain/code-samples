import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { compose } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import hydrate from '../higher-order-components/hydrate';
import { choosePerformance } from '../actionCreators';

function Performances({
  title,
  shortTitle,
  performances,
  concessions,
  dispatchChoosePerformance,
}) {
  return (
    <div>
      <h4 className="mb-3 text-center">{shortTitle || title}</h4>
      <div className="text-center mb-3">
        {'Concessions are available for performances marked with'} <i className="fa fa-dot-circle-o text-success " />.
      </div>
      <div className="row justify-content-center">
        <div className="col-sm-6">
          <div className="list-group">
            {performances.toJS().map((date) => {
              const momentDate = moment.utc(date);
              if (moment.utc(date).add(15, 'minutes').isBefore(moment.utc())) {
                return null;
              }

              return (
                <button
                  key={date}
                  className="list-group-item list-group-item-action text-center"
                  onClick={() => { dispatchChoosePerformance(date); }}
                >
                  {concessions.indexOf(date) !== -1 && (
                    <i className="fa fa-dot-circle-o text-success pull-right" />
                  )}
                  {momentDate.format('dddd Do MMMM YYYY')}
                  <br />
                  <strong>{momentDate.format('HH:mm')}</strong>

                </button>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

Performances.propTypes = {
  title: PropTypes.string,
  shortTitle: PropTypes.string,
  performances: ImmutablePropTypes.list,
  concessions: ImmutablePropTypes.list,
  dispatchChoosePerformance: PropTypes.func.isRequired,
};

Performances.defaultProps = {
  title: '',
  shortTitle: '',
  performances: [],
  concessions: [],
};

const mapStateToProps = state => ({
  production: state.getIn(['form', 'data', 'production']),
  title: state.getIn(['form', 'meta', 'production', 'title']),
  shortTitle: state.getIn(['form', 'meta', 'production', 'shortTitle']),
  performances: state.getIn(['form', 'meta', 'production', 'performances']),
  concessions: state.getIn(['form', 'meta', 'production', 'concessions']),
  weeks: state.getIn(['form', 'meta', 'production', 'weeks']),
});

const mapDispatchToProps = dispatch => ({
  dispatchChoosePerformance: performance => dispatch(choosePerformance({ performance })),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  hydrate([
    ['production', ownProps => `/productions/${ownProps.production}`],
  ]),
)(Performances);
