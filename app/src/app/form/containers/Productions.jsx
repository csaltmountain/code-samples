import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';

import hydrate from '../higher-order-components/hydrate';
import { chooseProduction } from '../actionCreators';

import '../styles/productions.scss';

const formatDate = date => moment(date).format('dddd Do MMMM YYYY');

function Productions({ productions, dispatchChooseProduction }) {
  if (productions.size === 0) {
    return (
      <div className="welcome text-center">
        <div className="display-3">Come back soon!</div>
        <p className="lead">
          {'Visit our '}
          <a href="https://www.facebook.com/saltmountainhq">Facebook page</a>
          {' for information about our next production.'}
        </p>
      </div>
    );
  }

  return (
    <div className="welcome">
      {productions.toJS().map(production => (
        <div key={production._id} className="card bg-light text-center p-3 mb-3">
          <div className="card-body">
            <div className="h1 mb-0">{production.title}</div>
            <div className="text-muted mb-3">by {production.author}</div>
            <p className="lead">
              {formatDate(production.openingNight)} – {formatDate(production.closingNight)}
            </p>
            <p>{production.description}</p>
            <button
              className="btn btn-primary btn-lg"
              onClick={() => dispatchChooseProduction(production._id)}
            >
              Book Tickets
            </button>
          </div>
        </div>
      ))}

      <div className="header__info text-center">
        <p className="mb-3">{"To book online you'll need:"}</p>
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="row justify-content-around">
              <div className="col-md-6 mb-3">
                <div className="card">
                  <div className="card-body">
                    <i className="fa fa-fw fa-3x fa-at text-primary mb-1" />
                    <p className="lead card-text">Email Address</p>
                  </div>
                </div>
              </div>
              <div className="col-md-6 mb-3">
                <div className="card">
                  <div className="card-body">
                    <i className="fa fa-fw fa-3x fa-credit-card text-primary mb-1" />
                    <p className="lead card-text">Credit/Debit Card</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="text-muted">
          <h4 className="mb-3">Alternative Booking Options</h4>
          <div className="row">
            <div className="col-sm">
              <p className="lead">
                <strong>In Person</strong>
                <br />
                Salt Mountain
              </p>
              <p>
                {'Open on Saturday mornings from 10:00 to 12:00'}
                <br />
                {"To book in person you'll need:"}
              </p>
              <div className="row justify-content-around">
                <div className="col-lg-6 mb-3">
                  <div className="card">
                    <div className="card-body">
                      <i className="fa fa-fw fa-3x fa-money mb-1" />
                      <p className="lead card-text">Cash/Cheque</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

Productions.propTypes = {
  productions: ImmutablePropTypes.list.isRequired,
  dispatchChooseProduction: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  productions: state.getIn(['form', 'meta', 'productions']),
});

const mapDispatchToProps = dispatch => ({
  dispatchChooseProduction: _id => dispatch(chooseProduction({ _id })),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  hydrate([['productions', () => '/productions?query={"active":true}']]),
)(Productions);
