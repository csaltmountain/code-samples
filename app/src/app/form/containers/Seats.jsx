import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { List } from 'immutable';
import { connect } from 'react-redux';
import { compose } from 'redux';
import moment from 'moment';

import hydrate from '../higher-order-components/hydrate';
import { toggleSeat } from '../actionCreators';
import { getMaximumSeats } from '../selectors';

import SeatingPlan from '../components/SeatingPlan';

import '../styles/seating-plan.scss';

function Seats({
  title,
  shortTitle,
  selected,
  performance,
  maximum,
  availability,
  dispatchToggleSeat,
}) {
  return (
    <div className="form__seats">
      <h4 className="mb-3 text-center">{shortTitle || title}
        <br />
        <small className="text-muted">{moment.utc(performance).format('dddd Do MMMM YYYY [at] HH:mm')}</small>
      </h4>
      {selected > maximum && (
        <div className="alert alert-warning text-center" role="alert">
          <div className="lead">{'You have too many seats selected'}</div>
          {`Either go back to the previous screen and increase the number of tickets or unselect ${selected - maximum} of the selected seats below.`}
        </div>
      )}
      {availability.size === 0 ? (
        <div className="text-center">
          <i className="fa fa-spinner fa-pulse fa-4x fa-fw" />
        </div>
      ) : (
        <div>
          <div className="display-4 text-center mb-3">{selected}/{maximum}</div>
          <SeatingPlan availability={availability.toJS()} handleClick={dispatchToggleSeat} />
        </div>
      )}
    </div>
  );
}

Seats.propTypes = {
  title: PropTypes.string,
  shortTitle: PropTypes.string,
  performance: PropTypes.string,
  selected: PropTypes.number.isRequired,
  maximum: PropTypes.number.isRequired,
  availability: ImmutablePropTypes.list,
  dispatchToggleSeat: PropTypes.func.isRequired,
};

Seats.defaultProps = {
  title: '',
  shortTitle: '',
  performance: '',
  availability: List(),
  selected: 0,
  maximum: 0,
};

const mapStateToProps = state => ({
  production: state.getIn(['form', 'data', 'production']),
  tickets: state.getIn(['form', 'data', 'tickets']),
  performance: state.getIn(['form', 'data', 'performance']),
  title: state.getIn(['form', 'meta', 'production', 'title']),
  shortTitle: state.getIn(['form', 'meta', 'production', 'shortTitle']),
  availability: state.getIn(['form', 'meta', 'availability']),
  selected: state.getIn(['form', 'data', 'seats']).size,
  maximum: getMaximumSeats(state),
});

const mapDispatchToProps = dispatch => ({
  dispatchToggleSeat: (rowIndex, seatIndex) => dispatch(toggleSeat({ rowIndex, seatIndex })),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  hydrate([
    ['availability', ownProps => `/bookings/availability?performance=${ownProps.performance}`, ownProps => ownProps.availability && ownProps.availability.size > 0],
  ]),
)(Seats);
