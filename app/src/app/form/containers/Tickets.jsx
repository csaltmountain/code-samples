import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { compose } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

import { updateInput, hydrateMeta } from '../actionCreators';
import { getConcessionsAvailable, getFamilyAvailable } from '../selectors';

import TicketInput from '../components/TicketInput';

function Tickets({
  title,
  shortTitle,
  performance,
  tickets,
  prices,
  concessionsAvailable,
  familyAvailable,
  ...passThroughProps
}) {
  return (
    <div>
      <h4 className="mb-3 text-center">{shortTitle || title}
        <br />
        <small className="text-muted">{moment.utc(performance).format('dddd Do MMMM YYYY [at] HH:mm')}</small>
      </h4>
      <div className="row justify-content-center">
        <div className="col-sm-4 mb-2">
          <TicketInput
            title="Full Price"
            path="full"
            count={tickets.get('full')}
            price={prices.get('full')}
            {...passThroughProps}
          />
        </div>
        {familyAvailable && (
          <div className="col-sm-4 mb-2">
            <TicketInput
              title="Family"
              subtitle="Two adults and two children"
              path="family"
              count={tickets.get('family')}
              price={prices.get('family')}
              disabled={!familyAvailable}
              {...passThroughProps}
            />
          </div>
        )}
        {concessionsAvailable && (
          <div className="col-sm-4">
            <TicketInput
              title="Concessions"
              path="concessions"
              count={tickets.get('concessions')}
              price={prices.get('concessions')}
              disabled={!concessionsAvailable}
              {...passThroughProps}
            />
          </div>
        )}
      </div>
    </div>
  );
}

Tickets.propTypes = {
  title: PropTypes.string,
  shortTitle: PropTypes.string,
  performance: PropTypes.string,
  tickets: ImmutablePropTypes.map,
  prices: ImmutablePropTypes.map,
  concessionsAvailable: PropTypes.bool,
  familyAvailable: PropTypes.bool,
};

Tickets.defaultProps = {
  title: '',
  shortTitle: '',
  performance: '',
  tickets: {
    full: 0,
    concessions: 0,
    family: 0,
  },
  prices: {
    full: 0,
    concessions: 0,
    family: 0,
  },
  concessionsAvailable: false,
  familyAvailable: false,
};

const mapStateToProps = state => ({
  production: state.getIn(['form', 'data', 'production']),
  tickets: state.getIn(['form', 'data', 'tickets']),
  performance: state.getIn(['form', 'data', 'performance']),
  title: state.getIn(['form', 'meta', 'production', 'title']),
  shortTitle: state.getIn(['form', 'meta', 'production', 'shortTitle']),
  prices: state.getIn(['form', 'meta', 'production', 'prices']),
  concessionsAvailable: getConcessionsAvailable(state),
  familyAvailable: getFamilyAvailable(state),
});

const mapDispatchToProps = dispatch => ({
  dispatchUpdateInput: (path, value) => dispatch(updateInput({ path, value })),
  dispatchHydrateMeta: (path, data) => dispatch(hydrateMeta({ path, data })),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(Tickets);
