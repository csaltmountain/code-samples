import React from 'react';
import axios from 'axios';
import { store } from '../../store';

import { hydrateMeta } from '../actionCreators';

function hydrate(options) {
  return (WrappedComponent) => {
    class Hydrated extends React.Component {
      constructor(props) {
        super(props);
        this.state = { loaded: false };
      }

      componentDidMount() {
        options.forEach(([path, url, condition = () => false]) => {
          if (!condition(this.props)) {
            return axios.get(url(this.props))
              .then(({ data }) => {
                store.dispatch(hydrateMeta({ path, data }));
                this.setState({ loaded: true });
              })
              .catch(err => console.log(err));
          }

          return this.setState({ loaded: true });
        });
      }

      render() {
        if (!this.state.loaded) {
          return (
            <div className="text-center">
              <i className="fa fa-spinner fa-pulse fa-4x fa-fw" />
            </div>
          );
        }

        return <WrappedComponent {...this.props} />;
      }
    }

    Hydrated.displayName = `Hydrated(${WrappedComponent.name})`;

    return Hydrated;
  };
}

export default hydrate;
