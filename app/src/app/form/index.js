import Booking from './containers/Booking';
import SeatingPlan from './components/SeatingPlan';
import reducer from './reducer';

export { Booking as default, SeatingPlan, reducer };
