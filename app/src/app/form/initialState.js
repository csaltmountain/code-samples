export default () => ({
  data: {},
  meta: {},
  ui: {
    stage: 'productions',
    stages: {
      productions: {
        title: 'Productions',
        subtitle: 'Which production would you like to see?',
        percentage: 0,
      },
      performances: {
        title: 'Performance',
        subtitle: 'On which night would you like to go?',
        percentage: 20,
        back: 'productions',
      },
      tickets: {
        title: 'Tickets',
        subtitle: 'How many tickets would you like?',
        percentage: 40,
        back: 'performances',
        next: 'seats',
      },
      seats: {
        title: 'Seats',
        subtitle: 'Where would you like to sit?',
        percentage: 60,
        back: 'tickets',
        next: 'checkout',
      },
      checkout: {
        title: 'Checkout',
        subtitle: 'Enter your details to complete booking…',
        percentage: 80,
        back: 'seats',
      },
      complete: {
        title: 'Booking Complete',
        percentage: 100,
        hideButtons: true,
      },
    },
    submittingForm: false,
    submittingFormFailed: false,
    submittingFormConflicted: false,
  },
});
