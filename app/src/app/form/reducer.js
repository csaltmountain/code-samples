import { fromJS, List, Map } from 'immutable';

import {
  HYDRATE_META,
  CHOOSE_PRODUCTION,
  CHOOSE_PERFORMANCE,
  UPDATE_INPUT,
  NAVIGATE,
  TOGGLE_SEAT,
  SEQUENCE_SEATS,
  SUBMITTING_FORM,
  SUBMITTING_FORM_SUCCEEDED,
  SUBMITTING_FORM_FAILED,
  SUBMITTING_FORM_CONFLICTED,
  RESET_FORM,
} from './actions';

import initialState from './initialState';

export default (state = fromJS(initialState()), { type, payload }) => {
  switch (type) {
    case HYDRATE_META: {
      return state.setIn(['meta', payload.path], fromJS(payload.data));
    }

    case CHOOSE_PRODUCTION: {
      return state.withMutations(map =>
        map
          .setIn(['ui', 'stage'], 'performances')
          .setIn(['data', 'production'], payload._id),
      );
    }

    case CHOOSE_PERFORMANCE: {
      return state.withMutations(map =>
        map
          .setIn(['ui', 'stage'], 'tickets')
          .setIn(['data', 'performance'], payload.performance)
          .setIn(['data', 'tickets'], Map())
          .setIn(['data', 'seats'], List())
          .setIn(['meta', 'availability'], List()),
      );
    }

    case NAVIGATE: {
      return state.setIn(['ui', 'stage'], payload.path);
    }

    case TOGGLE_SEAT: {
      const { row, seat, rowIndex, seatIndex, newStatus } = payload;
      return state.withMutations(map =>
        map
          .setIn(['meta', 'availability', rowIndex, seatIndex, 'status'], newStatus)
          .updateIn(['data', 'seats'], list =>
            (newStatus === 'selected' ? list.push(Map({ row, seat })) : list.filterNot(item => item.get('row') === row && item.get('seat') === seat)),
          )
          .updateIn(['data', 'seats'], list => list.sort((a, b) => {
            const aRow = a.get('row');
            const bRow = b.get('row');
            // If rows are equal, sort by seat number
            if (aRow === bRow) {
              return a.get('seat') > b.get('seat') ? 1 : -1;
            }
            return aRow > bRow ? 1 : -1;
          })),
      );
    }

    case SEQUENCE_SEATS: {
      const { availability, seats } = payload;
      return state.withMutations(map =>
        map
          .setIn(['ui', 'submittingFormConflicted'], false)
          .setIn(['meta', 'availability'], availability)
          .setIn(['data', 'seats'], seats)
          .updateIn(['data', 'seats'], list => list.sort((a, b) => {
            const aRow = a.get('row');
            const bRow = b.get('row');
            // If rows are equal, sort by seat number
            if (aRow === bRow) {
              return a.get('seat') > b.get('seat') ? 1 : -1;
            }
            return aRow > bRow ? 1 : -1;
          })),
      );
    }

    case UPDATE_INPUT: {
      return state.setIn(['data', ...payload.path], payload.value);
    }

    case SUBMITTING_FORM: {
      return state.withMutations(map =>
        map
          .setIn(['ui', 'submittingForm'], true)
          .setIn(['ui', 'submittingFormFailed'], false),
      );
    }

    case SUBMITTING_FORM_SUCCEEDED: {
      return state.withMutations(map =>
        map
          .set('data', fromJS(initialState().data))
          .set('complete', fromJS(payload.data))
          .setIn(['ui', 'submittingForm'], true)
          .setIn(['ui', 'stage'], 'complete'),
      );
    }

    case SUBMITTING_FORM_FAILED: {
      return state.withMutations(map =>
        map
          .setIn(['ui', 'submittingForm'], false)
          .setIn(['ui', 'submittingFormFailed'], true)
          .setIn(['ui', 'submittingFormFailedMessage'], (payload && payload.message) || ''),
      );
    }

    case SUBMITTING_FORM_CONFLICTED: {
      return state.withMutations(map =>
        map
          .setIn(['ui', 'submittingForm'], false)
          .setIn(['ui', 'submittingFormFailed'], false)
          .setIn(['ui', 'submittingFormConflicted'], true)
          .setIn(['data', 'seats'], List())
          .setIn(['meta', 'availability'], List()),
      );
    }

    case RESET_FORM: {
      return fromJS(initialState());
    }

    default: {
      return state;
    }
  }
};
