import { createSelector } from 'reselect';
import { List } from 'immutable';

export const getTotalTickets = createSelector(
  state => state.getIn(['form', 'data', 'tickets', 'full']),
  state => state.getIn(['form', 'data', 'tickets', 'family']),
  state => state.getIn(['form', 'data', 'tickets', 'concessions']),
  (full = 0, family = 0, concessions = 0) => full + family + concessions,
);

export const getMaximumSeats = createSelector(
  state => state.getIn(['form', 'data', 'tickets', 'full']),
  state => state.getIn(['form', 'data', 'tickets', 'family']),
  state => state.getIn(['form', 'data', 'tickets', 'concessions']),
  (full = 0, family = 0, concessions = 0) => full + (family * 4) + concessions,
);

export const getConcessionsAvailable = createSelector(
  state => state.getIn(['form', 'data', 'performance']),
  state => state.getIn(['form', 'meta', 'production', 'concessions']),
  (performance, concessions) => concessions.indexOf(performance) !== -1,
);

export const getFamilyAvailable = createSelector(
  state => state.getIn(['form', 'data', 'performance']),
  state => state.getIn(['form', 'meta', 'production', 'family']),
  (performance, concessions) => concessions.indexOf(performance) !== -1,
);

export const ticketsValid = createSelector(
  getTotalTickets,
  totalTickets => totalTickets > 0,
);

export const seatsValid = createSelector(
  getMaximumSeats,
  state => state.getIn(['form', 'data', 'seats']),
  (maximumSeats, seats) => maximumSeats === seats.size,
);

export const getSeats = createSelector(
  state => state.getIn(['form', 'data', 'seats']),
  (seats = List()) => seats.toJS().map(item => item.row + item.seat).join(', '),
);

export const getCheckoutTotal = createSelector(
  state => state.getIn(['form', 'data', 'tickets', 'full']),
  state => state.getIn(['form', 'data', 'tickets', 'family']),
  state => state.getIn(['form', 'data', 'tickets', 'concessions']),
  state => state.getIn(['form', 'meta', 'production', 'prices', 'full']),
  state => state.getIn(['form', 'meta', 'production', 'prices', 'family']),
  state => state.getIn(['form', 'meta', 'production', 'prices', 'concessions']),
  (fullTickets = 0, familyTickets = 0, concessionsTickets = 0, fullPrice = 0, familyPrice = 0, concessionsPrice = 0) =>
    (fullTickets * fullPrice) + (familyTickets * familyPrice) + (concessionsTickets * concessionsPrice),
);
