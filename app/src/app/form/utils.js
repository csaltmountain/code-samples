import { List, fromJS } from 'immutable';

export default (availability, seats) => {
  // Convert immutable lists to JS arrays
  const newAvailability = availability.toJS();
  let newSeats = seats.toJS();

  const flip = (row, rowIndex, seatIndex, direction, double, callback) => {
    const checkNextSeat = (index) => {
      const status = row[index + direction] && row[index + direction].status;
      if (status === 'selected') {
        return checkNextSeat(index + direction);
      } else if (status === 'available' || (status === undefined && double) || (status === 'booked' && double)) {
        const oldSeat = newAvailability[rowIndex][(index + direction) - direction];
        const newSeat = newAvailability[rowIndex][(seatIndex - direction)];

        oldSeat.status = 'available';
        newSeat.status = 'selected';

        newSeats = [...newSeats, { row: newSeat.row, seat: newSeat.seat }];

        newSeats = newSeats.filter(item =>
          !(item.row === oldSeat.row && item.seat === oldSeat.seat),
        );

        // Start row again
        return callback(newAvailability, rowIndex);
      }

      // Skip infinite loop
      return callback(newAvailability, rowIndex, seatIndex + 1);
    };

    return checkNextSeat(seatIndex);
  };

  const sequence = (rows, rowIndex = 0, seatIndex = 0) => {
    const row = rows[rowIndex];
    const seat = row[seatIndex];

    if (seat && seat.status === 'selected') {
      // Get status of two seats to the left and join
      const l = [row[seatIndex - 2] && row[seatIndex - 2].status, row[seatIndex - 1] && row[seatIndex - 1].status].join('/');
      // If left invalid
      if (l === '/available' || l === 'booked/available') {
        return flip(row, rowIndex, seatIndex, 1, false, sequence);
      }

      if (l === 'selected/available') {
        return flip(row, rowIndex, seatIndex - 2, -1, true, sequence);
      }

      // Get status of two seats to the right and join
      const r = [row[seatIndex + 1] && row[seatIndex + 1].status, row[seatIndex + 2] && row[seatIndex + 2].status].join('/');

      // If right invalid
      if (r === 'available/' || r === 'available/booked') {
        return flip(row, rowIndex, seatIndex, -1, false, sequence);
      }

      if (r === 'available/selected') {
        return flip(row, rowIndex, seatIndex + 2, 1, true, sequence);
      }
    }

    if (seatIndex < 12) {
      return sequence(rows, rowIndex, seatIndex + 1);
    }

    return rowIndex < 12 && sequence(rows, rowIndex + 1);
  };

  sequence(newAvailability);

  return { availability: List(fromJS(newAvailability)), seats: List(fromJS(newSeats)) };
};
