import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Switch, Route, Redirect } from 'react-router-dom';
import { StripeProvider } from 'react-stripe-elements';
import ReactGA from 'react-ga';
import axios from 'axios';

import '../vendor/bootstrap/bootstrap.scss';
import { history, store } from './store';

import Form from './form';
import Bookings from './bookings';
import Availability from './availability';
import { AdminHeader, SignIn } from './auth';

if (process.env.NODE_ENV === 'production') {
  ReactGA.initialize('UA-106735370-1');
}

axios.defaults.baseURL = '/api';

ReactDOM.render(
  <StripeProvider apiKey={process.env.STRIPE_PK}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div>
          <Route component={AdminHeader} />
          <Switch>
            <Route path="/sign-in" component={SignIn} />
            <Route path="/" exact component={Form} />
            <Route path="/bookings" component={Bookings} />
            <Route path="/availability" component={Availability} />
            <Redirect to="/" />
          </Switch>
        </div>
      </ConnectedRouter>
    </Provider>
  </StripeProvider>,
  document.getElementById('root'),
);
