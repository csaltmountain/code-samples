export const loadState = (item) => {
  try {
    const serializedState = sessionStorage.getItem(item);
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return undefined;
  }
};

export const saveState = (key, value) => {
  try {
    sessionStorage.setItem('auth', JSON.stringify(value));
  } catch (err) {
    console.error(err);
  }
};
