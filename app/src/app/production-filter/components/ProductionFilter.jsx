import React from 'react';
import { get } from 'axios';
import PropTypes from 'prop-types';

class ProductionFilter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      productions: [],
    };
  }

  componentDidMount() {
    get('/productions?select=title&sort={"openingNight":-1}')
      .then(({ data }) => {
        this.setState({ productions: data, selected: data[0]._id });
        this.props.onSelect(data[0]._id);
      });
  }

  render() {
    return (
      <div className="form-group mb-5">
        <label htmlFor="production-filter">Production</label>
        <select
          id="production-filter"
          className="form-control"
          onChange={event => this.props.onSelect(event.target.value)}
        >
          {this.state.productions.map(production => (
            <option key={production._id} value={production._id}>{production.title}</option>
          ))}
        </select>
      </div>
    );
  }
}

ProductionFilter.propTypes = {
  onSelect: PropTypes.func.isRequired,
};

export default ProductionFilter;
