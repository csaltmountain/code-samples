import { combineReducers } from 'redux-immutable';
import { routerReducer } from 'react-router-redux';

import { reducer as form } from './form';
import { reducer as auth } from './auth';
import { reducer as bookings } from './bookings';
import { reducer as availability } from './availability';

export default combineReducers({
  router: routerReducer,
  form,
  auth,
  bookings,
  availability,
});
