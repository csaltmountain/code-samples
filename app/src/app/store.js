import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import createHistory from 'history/createBrowserHistory';
import { routerMiddleware } from 'react-router-redux';

import rootReducer from './rootReducer';

const history = createHistory();

const middleware = applyMiddleware(
  thunkMiddleware,
  routerMiddleware(history),
);

const store = createStore(
  rootReducer,
  process.env.NODE_ENV === 'development' ? composeWithDevTools(middleware) : middleware,
);

export { history, store };
