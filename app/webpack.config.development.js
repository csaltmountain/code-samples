const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

const precss = require("precss");
const autoprefixer = require("autoprefixer");

const buildPath = path.resolve("build");
const publicPath = path.resolve("public");

module.exports = {
  entry: path.resolve("src/app/index.jsx"),
  resolve: {
    extensions: [".js", ".json", ".jsx"]
  },
  devtool: "inline-source-map",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["env", "react"],
            plugins: ["transform-object-rest-spread"]
          }
        }
      },
      {
        test: /\.(scss|css)$/,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "postcss-loader",
            options: {
              plugins: [precss, autoprefixer]
            }
          },
          {
            loader: "sass-loader",
            options: {
              includePaths: [path.resolve("src", "vendor", "bootstrap")]
            }
          }
        ]
      }
    ]
  },
  devServer: {
    contentBase: publicPath,
    compress: true,
    port: 3000,
    open: true,
    openPage: "",
    historyApiFallback: true,
    hot: true,
    host: "0.0.0.0",
    disableHostCheck: true,
    stats: "errors-only",
    proxy: {
      "/api": {
        target: "http://localhost:9000",
        pathRewrite: { "^/api": "" },
        headers: {
          // 'x-ssl-client-verify': 'SUCCESS',
          // 'x-ssl-client-s-dn': '/CN=Box Office Telephone',
        }
      }
    }
  },
  output: {
    filename: "static/js/bundle.js",
    path: buildPath,
    publicPath: "/"
  },
  plugins: [
    new BundleAnalyzerPlugin(),
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb/),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.EnvironmentPlugin({
      NODE_ENV: "development",
      STRIPE_PK: "pk_test_aCIhVkMY1iaM8KwEDURZBOl",
      ADMIN_HOSTNAME: "localhost"
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: `${publicPath}/index.html`
    })
  ]
};
