const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const precss = require('precss');
const autoprefixer = require('autoprefixer');

const buildPath = path.resolve('build');
const publicPath = path.resolve('public');

module.exports = {
  bail: true,
  entry: ['babel-polyfill', path.resolve('src/app/index.jsx')],
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react'],
            plugins: ['transform-object-rest-spread'],
          },
        },
      },
      {
        test: /\.(scss|css)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [precss, autoprefixer],
              },
            },
            {
              loader: 'sass-loader',
              options: {
                outputStyle: 'compressed',
                includePaths: [path.resolve('src', 'vendor', 'bootstrap')],
              },
            },
          ],
        }),
      },
    ],
  },
  output: {
    filename: 'js/[chunkhash:10].min.js',
    path: buildPath,
    publicPath: '/',
  },
  plugins: [
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb/),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production',
      ADMIN_HOSTNAME: 'admin.saltmountainhq.com',
      STRIPE_PK: '',
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        screw_ie8: true,
        warnings: false,
      },
      mangle: {
        screw_ie8: true,
      },
      output: {
        comments: false,
        screw_ie8: true,
      },
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: `${publicPath}/index.html`,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new ExtractTextPlugin('css/[contenthash:10].min.css'),
    new CleanWebpackPlugin(buildPath),
    new CopyWebpackPlugin([{ from: publicPath, to: buildPath }]),
  ],
};
